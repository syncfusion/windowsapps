﻿using System;
using System.Linq;
using System.Net.Http;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private async void GoButton_Click(object sender, RoutedEventArgs e)
        {
            // Request a random set of letters from http://www/random.org
            // For details, see http://www.random.org/clients/http/

            // 1 string result
            // variable character length based opn UI input
            // variable includes digits based on UI input
            // variable upper and lower alpha based on UI input
            // request unique results
            // request results as plain text
            // request should initiate a new randomization

            // Build the Uri from the inputs in the UI
            var builder = new UriBuilder("http://www.random.org/strings/");
            var lengthArg = String.Format("len={0}", LengthSelection.SelectedItem);
            var digitsArg = String.Format("digits={0}", IncludeDigits.IsOn ? "on" : "off");
            var includeUpper = UpperCaseOnly.IsChecked.Value || MixedCase.IsChecked.Value;
            var upperAlphaArg = String.Format("upperalpha={0}", includeUpper ? "on" : "off");
            var includeLower = LowerCaseOnly.IsChecked.Value || MixedCase.IsChecked.Value;
            var lowerAlphaArg = String.Format("loweralpha={0}", includeLower ? "on" : "off");
            var queryString =
                String.Format("num=1&{0}&{1}&{2}&{3}&unique=on&format=plain&rnd=new",
                lengthArg,
                digitsArg,
                upperAlphaArg,
                lowerAlphaArg);
            builder.Query = queryString;

            // Make the Http request
            var httpClient = new HttpClient();
            var results = await httpClient.GetStringAsync(builder.Uri);

            // Split the first result off from the (potential) list of results
            var resultWord = results
                .Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries)
                .First();

            // Report the word in the UI
            RandomWordText.Text = String.Format("Your random word is: {0}", resultWord);
        }
    }
}
