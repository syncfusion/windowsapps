﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class DemoAppBarPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public DemoAppBarPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void AppBarListView_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            // If any items are selected, force the app bar to be "sticky"
            // and display it.  Otherwise, clear the "sticky" behavior and
            // collapse the bar.
            if (AppBarListView.SelectedItems.Any())
            {
                this.BottomAppBar.IsSticky = true;
                this.BottomAppBar.IsOpen = true;
            }
            else
            {
                this.BottomAppBar.IsOpen = false;
                this.BottomAppBar.IsSticky = false;
            }
        }
    }

    public static class AppBarExtensions
    {
        [Flags]
        public enum AppBarDisplayFlags
        {
            None = 0,
            Bottom = 1,
            Top = 2,
            BottomAndTop = 3,
        }

        public static readonly DependencyProperty AppBarDisplayOnListSelectionProperty =
            DependencyProperty.RegisterAttached(
                "AppBarDisplayOnListSelection",
                typeof(AppBarDisplayFlags),
                typeof(Selector),
                new PropertyMetadata(AppBarDisplayFlags.None, OnAppBarDisplayOnListSelectionChanged));

        public static void SetAppBarDisplayOnListSelection(Selector element, AppBarDisplayFlags value)
        {
            element.SetValue(AppBarDisplayOnListSelectionProperty, value);
        }

        public static AppBarDisplayFlags GetAppBarDisplayOnListSelection(Selector element)
        {
            return (AppBarDisplayFlags)element.GetValue(AppBarDisplayOnListSelectionProperty);
        }

        private static void OnAppBarDisplayOnListSelectionChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs dependencyPropertyChangedEventArgs)
        {
            // Don't hook up the event listeners when running in the designer
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return;

            // Identify the selector to which this property has been applied (if none, then do nothing)
            var selector = dependencyObject as Selector;
            if (selector == null) return;
            var selectedFlags = (AppBarDisplayFlags)dependencyPropertyChangedEventArgs.NewValue;
            HookEvents(selector, selectedFlags);
        }

        private static void HookEvents(Selector selector, AppBarDisplayFlags flags)
        {
            if (selector == null) throw new ArgumentNullException("selector");

            // Clear any "active" event handlers
            UnhookEvents(selector);

            if (flags != AppBarDisplayFlags.None)
            {
                selector.Unloaded += HandleUnloaded;
                selector.SelectionChanged += HandleSelectionChanged;
            }
        }

        private static void UnhookEvents(Selector selector)
        {
            if (selector == null) throw new ArgumentNullException("selector");

            selector.Unloaded -= HandleUnloaded;
            selector.SelectionChanged -= HandleSelectionChanged;
        }

        private static void HandleUnloaded(Object sender, RoutedEventArgs e)
        {
            UnhookEvents((Selector)sender);
        }

        private static void HandleSelectionChanged(Object sender, SelectionChangedEventArgs e)
        {
            var selector = sender as Selector;
            if (selector == null) return;

            // Traverse the selector's parents to find the firet "page" element
            var containingPage = selector.GetVisualAncestors().OfType<Page>().FirstOrDefault();
            if (containingPage == null) return;
            var currentFlags = GetAppBarDisplayOnListSelection(selector);
            var showBottomAppBar = (currentFlags & AppBarDisplayFlags.Bottom) == AppBarDisplayFlags.Bottom;
            var showTopAppBar = (currentFlags & AppBarDisplayFlags.Top) == AppBarDisplayFlags.Top;

            if (selector.SelectedItem != null)
            {
                // An item has been selected - show the relevant app bars
                if (showBottomAppBar) ShowAppBar(containingPage.BottomAppBar);
                if (showTopAppBar) ShowAppBar(containingPage.TopAppBar);
            }
            else
            {
                // Nothing has been selected - hide the relevant app bars
                if (showBottomAppBar) HideAppBar(containingPage.BottomAppBar);
                if (showTopAppBar) HideAppBar(containingPage.TopAppBar);
            }
        }

        private static void ShowAppBar(AppBar appBar)
        {
            if (appBar == null) return;
            appBar.IsSticky = true;
            appBar.IsOpen = true;
        }

        private static void HideAppBar(AppBar appBar)
        {
            if (appBar == null) return;
            appBar.IsOpen = false;
            appBar.IsSticky = false;
        }

        /// <summary>
        /// Gets the ancestors of the element, up to the root.
        /// </summary>
        /// <param name="node">The element to start from.</param>
        /// <returns>An enumerator of the ancestors.</returns>
        public static IEnumerable<FrameworkElement> GetVisualAncestors(this FrameworkElement node)
        {
            var parent = node.GetVisualParent();
            while (parent != null)
            {
                yield return parent;
                parent = parent.GetVisualParent();
            }
        }

        /// <summary>
        /// Gets the visual parent of the element.
        /// </summary>
        /// <param name="node">The element to check.</param>
        /// <returns>The visual parent.</returns>
        public static FrameworkElement GetVisualParent(this FrameworkElement node)
        {
            return VisualTreeHelper.GetParent(node) as FrameworkElement;
        }
    }
}
