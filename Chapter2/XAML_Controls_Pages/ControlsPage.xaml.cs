﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class DemoControlsPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public DemoControlsPage()
        {
            this.InitializeComponent();
            DataContext = this;
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private readonly ObservableCollection<String> _simpleItems = new ObservableCollection<String>();
        public IEnumerable<String> SimpleItems
        {
            get { return _simpleItems; }
        }

        private readonly IEnumerable<Person> _complexItems = new[] 
        {
            new Person{LastName="Garland", FirstName="John"},
            new Person{LastName="Likness", FirstName="Jeremy"},
            new Person{LastName="Prosise", FirstName="Jeff"},
        };
        public IEnumerable<Person> ComplexItems
        {
            get { return _complexItems; }
        }

        private void HandleAddItemButtonClick(object sender, RoutedEventArgs e)
        {
            _simpleItems.Add("Item " + (_simpleItems.Count() + 1));
        }

        private void HandleBrowseToURLClick(object sender, RoutedEventArgs e)
        {
            // Code to validate the supplied URL omitted for brevity
            var targetUrl = new Uri(BrowserURL.Text);
            WebViewDemo.Navigate(targetUrl);
        }

        private async void HandleShowMessageDialogButtonClick(Object sender, RoutedEventArgs e)
        {
            // PopupMenu - up to 6 items - can be UICommands or UICommandSeparator()
            var popupMenu = new PopupMenu();
            popupMenu.Commands.Add(new UICommand("Show a Popup", command =>
                {
                    var invokingItemRect = GetRect(sender);
                    var midpoint = new Point((invokingItemRect.Left + invokingItemRect.Right) / 2.0, (invokingItemRect.Top + invokingItemRect.Bottom) / 2.0);
                    
                    // Create a new popup with the content set from the PopupContentControl custom control
                    var ctrlLeft = invokingItemRect.Left + (invokingItemRect.Width / 2.0) - (250 / 2.0);
                    var ctrlTop = invokingItemRect.Top + (invokingItemRect.Height / 2.0) - (150 / 2.0);

                    // Create the popup
                    var popup = new Popup()
                    {
                        Child = new DemoPopupContentControl(),
                        HorizontalOffset = ctrlLeft,  // Distance to left side of app window
                        VerticalOffset = ctrlTop,  // Distance to top of app window
                        IsLightDismissEnabled = true,
                    };

                    // Event handlers for Opened & Closed events
                    popup.Opened += (o, args) => { };
                    popup.Closed += (o, args) => { };

                    // Add the popup to the page's layout root to accomodate the onscreen keyboard
                    LayoutRoot.Children.Add(popup);
                    
                    // Display the popup
                    popup.IsOpen = true;
                }));

            // Show a separator
            popupMenu.Commands.Add(new UICommand("Then Show a Separator"));
            popupMenu.Commands.Add(new UICommandSeparator());

            // Show a Message Box
            popupMenu.Commands.Add(new UICommand("Show a Message Box", async command =>
                {
                    var dialog = new MessageDialog("Message Box Contents", "Message Box Title");
                    await dialog.ShowAsync();
                    // Not shown - options for specifying custom commands to display 
                    // using the Commands Property and the UICommands class
                }));

            // In addition to the callback selected command's callback (if it has one), the Show
            // command will either return the selected command or null if dismissed without a command
            var result = await popupMenu.ShowForSelectionAsync(GetRect(sender), Placement.Default);//Above, Below, Left, Right
            if (result == null)
            {
                
            }
            else
            {
                var selectedItem = result.Label;
            }
            //await popupMenu.ShowAsync(point);
        }

        private Rect GetRect(Object sender)
        {
            var element = (FrameworkElement)sender;
            var elementTransform = element.TransformToVisual(null);
            var point = elementTransform.TransformPoint(new Point());
            return new Rect(point, new Size(element.ActualWidth, element.ActualHeight));
        }
    }
}
