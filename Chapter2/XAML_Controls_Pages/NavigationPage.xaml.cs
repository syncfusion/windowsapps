﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class DemoNavigationPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public DemoNavigationPage()
        {
            this.InitializeComponent();
            this.Loaded += (o, e) =>
            {
                System.Diagnostics.Debug.WriteLine("Loaded Event Fired");

                Frame.Navigating += (sender, args) =>
                {
                    //args.NavigationMode == NavigationMode.Refresh.Forward.New.Back
                    //var sourcePageType = args.SourcePageType;
                    //args.Cancel = true;
                };

                Frame.Navigated += (sender, args) =>
                {
                    var content = args.Content; // Root node of the target page's content
                    //args.NavigationMode
                    //args.SourcePageType
                    //var uri = args.Uri; // Useless
                    var param = args.Parameter;
                };

                // On nav error Frame.NavigationFailed
                // On Nav requested while another is in progress Frame.NavigationStopped
            };
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            NavigationDemoHelper.Instance.Clear();
        }

        protected override void OnNavigatingFrom(NavigatingCancelEventArgs e)
        {
            base.OnNavigatingFrom(e);
            NavigationDemoHelper.Instance.AddNavigationStep(GetType().Name + " - OnNavigatingFrom");
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
            NavigationDemoHelper.Instance.AddNavigationStep(GetType().Name + " - OnNavigatedFrom");
        }

        private void HandleNavigateClick(object sender, RoutedEventArgs e)
        {
            // Navigate to an instance of the DemoNavigationPage without a navigation parameter
            Frame.Navigate(typeof(DemoNavigationPageTarget));
        }

        private void HandleNavigateWithParamsClick(object sender, RoutedEventArgs e)
        {
            // Navigate to an instance of the DemoNavigationPage with a navigation parameter
            Frame.Navigate(typeof(DemoNavigationPageTarget), "Navigation parameter");
        }

        private void HandleNavigateBackClick(object sender, RoutedEventArgs e)
        {
            // Navigate back, if possible
            if (Frame.CanGoBack)
            {
                Frame.GoBack();
            }
        }
    }

    public class NavigationDemoHelper
    {
        private static NavigationDemoHelper _instance;

        public static NavigationDemoHelper Instance
        {
            get { return _instance ?? (_instance = new NavigationDemoHelper()); }
        }

        private readonly List<String> _navigationHistory = new List<String>();

        private NavigationDemoHelper()
        {
        }

        public void Clear()
        {
            _navigationHistory.Clear();
        }

        public void AddNavigationStep(String step)
        {
            _navigationHistory.Add(step);
        }

        public IEnumerable<String> CurrentHistory { get { return _navigationHistory; } }
    }
}
