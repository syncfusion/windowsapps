﻿<common:LayoutAwarePage
    x:Name="pageRoot"
    x:Class="WindowsStoreAppsSuccinctly.DemoGridViewPage"
    DataContext="{Binding DefaultViewModel, RelativeSource={RelativeSource Self}}"
    xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
    xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
    xmlns:local="using:WindowsStoreAppsSuccinctly"
    xmlns:common="using:WindowsStoreAppsSuccinctly.Common"
    xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    mc:Ignorable="d">

    <Page.Resources>

        <!-- TODO: Delete this line if the key AppName is declared in App.xaml -->
        <!--<x:String x:Key="AppName">My Application</x:String>-->
        <!-- Grid-appropriate 225 pixel square item template -->
        <DataTemplate x:Key="Standard225x225ItemTemplate">
            <Grid HorizontalAlignment="Left" Width="225" Height="225">
                <Border Background="{StaticResource ListViewItemPlaceholderBackgroundThemeBrush}">
                    <Image Source="{Binding Image}" Stretch="UniformToFill" AutomationProperties.Name="{Binding Title}"/>
                </Border>
                <StackPanel VerticalAlignment="Bottom" Background="{StaticResource ListViewItemOverlayBackgroundThemeBrush}">
                    <TextBlock Text="{Binding Title}" Foreground="{StaticResource ListViewItemOverlayForegroundThemeBrush}" Style="{StaticResource TitleTextStyle}" Height="60" Margin="15,0,15,0"/>
                    <TextBlock Text="{Binding Subtitle}" Foreground="{StaticResource ListViewItemOverlaySecondaryForegroundThemeBrush}" Style="{StaticResource CaptionTextStyle}" TextWrapping="NoWrap" Margin="15,0,15,10"/>
                </StackPanel>
            </Grid>
        </DataTemplate>

        <!-- Grid-appropriate 200 pixel square item template -->
        <DataTemplate x:Key="Standard200x200ItemTemplate">
            <Grid HorizontalAlignment="Left" Width="200" Height="200">
                <Border Background="{StaticResource ListViewItemPlaceholderBackgroundThemeBrush}">
                    <Image Source="{Binding Image}" Stretch="UniformToFill" AutomationProperties.Name="{Binding Title}"/>
                </Border>
                <StackPanel VerticalAlignment="Bottom" Background="{StaticResource ListViewItemOverlayBackgroundThemeBrush}">
                    <TextBlock Text="{Binding Title}" Foreground="{StaticResource ListViewItemOverlayForegroundThemeBrush}" Style="{StaticResource TitleTextStyle}" Height="60" Margin="15,0,15,0"/>
                    <TextBlock Text="{Binding Subtitle}" Foreground="{StaticResource ListViewItemOverlaySecondaryForegroundThemeBrush}" Style="{StaticResource CaptionTextStyle}" TextWrapping="NoWrap" Margin="15,0,15,10"/>
                </StackPanel>
            </Grid>
        </DataTemplate>

        <!-- Define a CollectionViewSource bound to the GroupedItems collection. -->
        <!-- The ItemsPath property defines where items are located within each group. -->
        <CollectionViewSource
            x:Name="GroupedItemsViewSource"
            Source="{Binding GroupedItems}"
            IsSourceGrouped="true"
            ItemsPath="Items"/>

        <common:BooleanToVisibilityConverter x:Key="IfTrueThenVisible"/>
        <common:BooleanToVisibilityConverter VisibleValue="False" x:Key="IfFalseThenVisible"/>
    </Page.Resources>

    <!--
        This grid acts as a root panel for the page that defines two rows:
        * Row 0 contains the back button and page title
        * Row 1 contains the rest of the page layout
    -->
    <Grid Style="{StaticResource LayoutRootStyle}">
        <Grid.RowDefinitions>
            <RowDefinition Height="140"/>
            <RowDefinition Height="*"/>
        </Grid.RowDefinitions>

        <!-- Back button and page title -->
        <Grid>
            <Grid.ColumnDefinitions>
                <ColumnDefinition Width="Auto"/>
                <ColumnDefinition Width="*"/>
            </Grid.ColumnDefinitions>
            <Button x:Name="backButton" Click="GoBack" IsEnabled="{Binding Frame.CanGoBack, ElementName=pageRoot}" Style="{StaticResource BackButtonStyle}"/>
            <TextBlock x:Name="pageTitle" Grid.Column="1" Text="{StaticResource AppName}" Style="{StaticResource PageHeaderTextStyle}"/>
        </Grid>

        <Grid Grid.Row="1">
            <Grid.RowDefinitions>
                <RowDefinition Height="Auto"/>
                <RowDefinition Height="*"/>
            </Grid.RowDefinitions>
            <StackPanel Orientation="Horizontal" Margin="120,0,0,0">
                <ToggleSwitch x:Name="ShowGroupingToggle" 
                              Header="Show Grouping"
                              IsOn="False"
                              OffContent="UnGrouped"
                              OnContent="Grouped"/>

                <ToggleSwitch x:Name="ItemClickEnabledToggle" 
                              Margin="20,0,0,0"
                              Header="Is Item Click Enabled"
                              IsOn="True"/>

                <StackPanel Margin="20,0,0,0">
                    <TextBlock Text="Selection Mode: " Style="{StaticResource BasicTextStyle}" Margin="0,5,0,2"/>
                    <ComboBox x:Name="SelectionModeComboBox" Width="250" HorizontalAlignment="Left" SelectionChanged="HandleSelectionModeComboBoxSelectionChanged">
                        <x:String>None</x:String>
                        <x:String>Single</x:String>
                        <x:String>Multiple</x:String>
                        <x:String>Extended</x:String>
                    </ComboBox>
                </StackPanel>
                
                <TextBlock x:Name="SelectedItemCount" Text ="0 Items Selected" Style="{StaticResource SubheaderTextStyle}" VerticalAlignment="Bottom" Margin="20,0,0,20"/>

            </StackPanel>

            <GridView Grid.Row="1" 
                      x:Name="DemoGridView"
                      Padding="120,0,40,50"
                      ItemsSource="{Binding AllItems}"
                      ItemTemplate="{StaticResource Standard225x225ItemTemplate}"
                      SelectionMode="None"
                      IsItemClickEnabled="{Binding IsOn, ElementName=ItemClickEnabledToggle, Mode=TwoWay}"
                      SelectionChanged="HandleGridViewSelectionChanged"
                      ItemClick="HandleGridViewItemClick"
                      Visibility="{Binding IsOn, ElementName=ShowGroupingToggle, Converter={StaticResource IfFalseThenVisible}}">
            </GridView>

            <GridView Grid.Row="1" 
                      x:Name="DemoGroupedGridView"
                      Padding="120,0,40,50"
                      ItemsSource="{Binding Source={StaticResource GroupedItemsViewSource}}"
                      ItemTemplate="{StaticResource Standard200x200ItemTemplate}"
                      SelectionMode="None"
                      IsItemClickEnabled="{Binding IsOn, ElementName=ItemClickEnabledToggle, Mode=TwoWay}"
                      SelectionChanged="HandleGridViewSelectionChanged"
                      ItemClick="HandleGridViewItemClick"
                      Visibility="{Binding IsOn, ElementName=ShowGroupingToggle, Converter={StaticResource IfTrueThenVisible}}">
                <GridView.GroupStyle>
                    <GroupStyle>
                        <GroupStyle.HeaderTemplate>
                            <DataTemplate>
                                <Grid Margin="1,0,0,6">
                                    <Button Click="HandleGroupedHeaderClick"
                                            Style="{StaticResource TextPrimaryButtonStyle}" >
                                        <StackPanel Orientation="Horizontal">
                                            <TextBlock Text="{Binding Title}" Margin="3,-7,10,10" Style="{StaticResource GroupHeaderTextStyle}" />
                                            <TextBlock Text="{StaticResource ChevronGlyph}" FontFamily="Segoe UI Symbol" Margin="0,-7,0,10" Style="{StaticResource GroupHeaderTextStyle}"/>
                                        </StackPanel>
                                    </Button>
                                </Grid>
                            </DataTemplate>
                        </GroupStyle.HeaderTemplate>
                        <GroupStyle.Panel>
                            <ItemsPanelTemplate>
                                <VariableSizedWrapGrid Orientation="Vertical" Margin="0,0,80,0"/>
                            </ItemsPanelTemplate>
                        </GroupStyle.Panel>
                    </GroupStyle>
                </GridView.GroupStyle>
            </GridView>
        </Grid>

        <VisualStateManager.VisualStateGroups>

            <!-- Visual states reflect the application's view state -->
            <VisualStateGroup x:Name="ApplicationViewStates">
                <VisualState x:Name="FullScreenLandscape"/>
                <VisualState x:Name="Filled"/>

                <!-- The entire page respects the narrower 100-pixel margin convention for portrait -->
                <VisualState x:Name="FullScreenPortrait">
                    <Storyboard>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="backButton" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource PortraitBackButtonStyle}"/>
                        </ObjectAnimationUsingKeyFrames>
                    </Storyboard>
                </VisualState>

                <!-- The back button and title have different styles when snapped -->
                <VisualState x:Name="Snapped">
                    <Storyboard>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="backButton" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource SnappedBackButtonStyle}"/>
                        </ObjectAnimationUsingKeyFrames>
                        <ObjectAnimationUsingKeyFrames Storyboard.TargetName="pageTitle" Storyboard.TargetProperty="Style">
                            <DiscreteObjectKeyFrame KeyTime="0" Value="{StaticResource SnappedPageHeaderTextStyle}"/>
                        </ObjectAnimationUsingKeyFrames>
                    </Storyboard>
                </VisualState>
            </VisualStateGroup>
        </VisualStateManager.VisualStateGroups>
    </Grid>
</common:LayoutAwarePage>
