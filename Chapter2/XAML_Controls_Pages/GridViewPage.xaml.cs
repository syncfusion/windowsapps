﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using WindowsStoreAppsSuccinctly.Data;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class DemoGridViewPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public DemoGridViewPage()
        {
            this.InitializeComponent();
            DataContext = this;
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        public IEnumerable<SampleDataItem> AllItems
        {
            get
            {
                var results = SampleDataSource
                    .GetGroups("AllGroups")
                    .SelectMany(x => x.Items)
                    .ToList();
                return results;
            }
        }

        public IEnumerable<SampleDataGroup> GroupedItems
        {
            get { return SampleDataSource.GetGroups("AllGroups"); }
        }

        private async void HandleGridViewItemClick(object sender, ItemClickEventArgs e)
        {
            // Determine the data item that was clicked
            var clickedItem = (SampleDataItem)e.ClickedItem;

            // Show a message indicating the clicked item
            var message = String.Format("Item {0} was clicked", clickedItem.Title);
            var clickMessage = new MessageDialog(message, "GridView Demo");
            await clickMessage.ShowAsync();
        }

        private void HandleGridViewSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // From the arguments, determine new items selected/deselected
            var newlySelectedItems = e.AddedItems;
            var deselectedItems = e.RemovedItems;

            // Get the comprehensive list of selected items, depending on which grid is being displayed
            var allSelectedItems = ShowGroupingToggle.IsOn ? DemoGroupedGridView.SelectedItems : DemoGridView.SelectedItems;

            SelectedItemCount.Text = String.Format("{0} Items Selected", allSelectedItems.Count());
        }

        private void HandleSelectionModeComboBoxSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectionMode = ListViewSelectionMode.None;
            if (SelectionModeComboBox.SelectedItem != null)
            {
                Enum.TryParse<ListViewSelectionMode>(SelectionModeComboBox.SelectedItem.ToString(), out selectionMode);
            }
            DemoGridView.SelectionMode = selectionMode;
            DemoGroupedGridView.SelectionMode = selectionMode;
        }

        private async void HandleGroupedHeaderClick(object sender, RoutedEventArgs e)
        {
            // Determine what group the Button instance represents
            var clickedItem = (SampleDataGroup)((FrameworkElement)sender).DataContext;
            var clickMessage = new MessageDialog("Item " + clickedItem.Title + " was clicked", "Grouped GridView Demo");
            await clickMessage.ShowAsync();
        }
    }
}
