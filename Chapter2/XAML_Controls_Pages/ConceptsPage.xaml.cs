﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class DemoConceptsPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public DemoConceptsPage()
        {
            this.InitializeComponent();
            this.DataContext = new Person
            {
                LastName = "Garland",
                FirstName = "John",
                IsEditable = true
            };
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        // The related event handler in the code behind file
        private async void EventDemoButton_Click_1(object sender, RoutedEventArgs e)
        {
            await new MessageDialog("The demo button was clicked.").ShowAsync();
        }

        private void AnimateButton_Click_1(object sender, RoutedEventArgs e)
        {
            // Locate the storyboard as a resource on the page object
            var storyboard = (Storyboard)this.Resources["DemoStoryboard"];
            // Unhook the storyboard completed event to keep from hooking it multiple times
            storyboard.Completed -= OnStoryboardCompleted;
            // Subscribe to the storyboard completed event
            storyboard.Completed += OnStoryboardCompleted;

            // If the storyboard is stopped, start it. Otherwise, stop it.
            if (storyboard.GetCurrentState() == ClockState.Stopped)
            {
                storyboard.Begin();
            }
            else
            {
                storyboard.Stop();
            }
        }

        private async void OnStoryboardCompleted(object sender, object e)
        {
            var storyboard = sender as Storyboard;
            if (storyboard != null)
            {
                var messageDialog = new MessageDialog("Animation Completed", "XAML Concepts");
                await messageDialog.ShowAsync();
            }
        }

        private void ThemeTransitionButton_Click_1(object sender, RoutedEventArgs e)
        {
            ThemeTransitionPanel.Children.Add(new TextBlock { Text = "Item Number" + (ThemeTransitionPanel.Children.Count() + 1) });
        }

        //// Using a DependencyProperty as the backing store for SampleDependencyProperty.  
        //// This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty SampleDependencyPropertyProperty =
        //    DependencyProperty.Register(
        //    "SampleDependencyProperty", 
        //    typeof(Int32), 
        //    typeof(DemoBlankPage), 
        //    new PropertyMetadata(0, changeCallback));

        //public Int32 SampleDependencyProperty
        //{
        //    get { return (Int32)GetValue(SampleDependencyPropertyProperty); }
        //    set { SetValue(SampleDependencyPropertyProperty, value); }
        //}




        //// Using a DependencyProperty as the backing store for MyAttachedProperty.  
        //// This enables animation, styling, binding, etc...
        //public static readonly DependencyProperty MyAttachedPropertyProperty =
        //    DependencyProperty.RegisterAttached(
        //    "MyAttachedProperty",
        //    typeof(Int32),
        //    typeof(DemoBlankPage),
        //    new PropertyMetadata(0, changeCallback));

        //public static Int32 GetMyAttachedProperty(DependencyObject obj)
        //{
        //    return (Int32)obj.GetValue(MyAttachedPropertyProperty);
        //}

        //public static void SetMyAttachedProperty(DependencyObject obj, Int32 value)
        //{
        //    obj.SetValue(MyAttachedPropertyProperty, value);
        //}
    }

    // Utility class to demonstrate DataContext and binding concepts
    public class Person : INotifyPropertyChanged
    {
        private String _firstName;
        public String FirstName
        {
            get { return _firstName; }
            set
            {
                _firstName = value;
                OnPropertyChanged();
            }
        }

        private String _lastName;
        public String LastName
        {
            get { return _lastName; }
            set
            {
                _lastName = value;
                OnPropertyChanged();
            }
        }

        private Boolean _isEditable;
        public Boolean IsEditable
        {
            get { return _isEditable; }
            set
            {
                _isEditable = value;
                OnPropertyChanged();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        private void OnPropertyChanged([CallerMemberName]String caller = null)
        {
            PropertyChanged(this, new PropertyChangedEventArgs(caller));
        }
    }

}
