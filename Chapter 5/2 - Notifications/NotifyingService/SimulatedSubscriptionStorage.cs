﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NotifyingService
{
    public static class SimulatedSubscriptionStorage
    {
        private readonly static IList<Subscription> Subscriptions = new List<Subscription>();

        public static void UpdateSubscription(Guid subscriberId, String subscriptionUri, String zipCodeOfInterest)
        {
            var existingSubscription = Subscriptions.FirstOrDefault(x => x.SubscriberId == subscriberId);
            if (existingSubscription == null)
            {
                // New Subscription
                var subscription = new Subscription
                {
                    SubscriberId = subscriberId,
                    SubscriptionUri = subscriptionUri,
                    ZipCodeOfInterest = zipCodeOfInterest
                };
                Subscriptions.Add(subscription);
            }
            else
            {
                existingSubscription.SubscriptionUri = subscriptionUri;
                existingSubscription.ZipCodeOfInterest = zipCodeOfInterest;
            }
        }

        public static IEnumerable<Subscription> GetMatchingSubscriptions(String zipCodeOfInterest)
        {
            return Subscriptions.Where(x => x.ZipCodeOfInterest == zipCodeOfInterest).ToArray();
        }

        public static void RemoveBadSubscription(Subscription matchingSubscription)
        {
            Subscriptions.Remove(matchingSubscription);
        }
    }
}