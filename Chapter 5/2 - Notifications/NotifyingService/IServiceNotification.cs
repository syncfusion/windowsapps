﻿using System;
using System.ServiceModel;

namespace NotifyingService
{
    // Use the WCF Test Client to invoke these methods
    [ServiceContract]
    public interface IServiceNotification
    {
        [OperationContract]
        void PublishToastNotification(String zipCodeOfInterest);

        [OperationContract]
        void PublishTileUpdate(String zipCodeOfInterest);

        [OperationContract]
        void PublishBadgeUpdate(String zipCodeOfInterest);

        [OperationContract]
        void PublishRawNotification(String zipCodeOfInterest);
    }
}