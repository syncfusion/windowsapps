﻿using System;
using System.ServiceModel;

namespace NotifyingService
{
    /// <summary>
    /// Contract to define the behavior available to service subscribers
    /// </summary>
    [ServiceContract]
    public interface IServiceSubscription
    {
        /// <summary>
        /// A sample endpoint that updates subscriptions, including a Zip Code as data for triggering notifications
        /// </summary>
        /// <param name="subscriberId"></param>
        /// <param name="subscriptionUri"></param>
        /// <param name="zipCodeOfInterest"></param>
        [OperationContract]
        void UpdateSubscription(Guid subscriberId, String subscriptionUri, String zipCodeOfInterest);
    }
}
