﻿using System;

namespace NotifyingService
{
    public class Subscription
    {
        public Guid SubscriberId { get; set; }
        public String SubscriptionUri { get; set; }
        public String ZipCodeOfInterest { get; set; }
    }
}