﻿using System;
using System.Net;
using NotificationsExtensions;
using NotificationsExtensions.BadgeContent;
using NotificationsExtensions.RawContent;
using NotificationsExtensions.TileContent;
using NotificationsExtensions.ToastContent;

namespace NotifyingService
{
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service.svc or Service.svc.cs at the Solution Explorer and start debugging.
    public class Service: IServiceSubscription, IServiceNotification
    {
        public void UpdateSubscription(Guid subscriberId, String subscriptionUri, String zipCodeOfInterest)
        {
            SimulatedSubscriptionStorage.UpdateSubscription(subscriberId, subscriptionUri, zipCodeOfInterest);
        }

        // WNS authentication values obtained from application dashboard values
        // TODO - Substitite your own values from the store
        private const String CliendSID = "YOUR CLIENT SID HERE";
        private const String ClientSecret = "YOUR CLIENT SECRET HERE";

        //public void PublishNotifications(String dataOfInterest)
        //{
        //    // Set up the token provider to provide the access token for notification requests
        //    var tokenProvider = new WnsAccessTokenProvider(CliendSID, ClientSecret);

        //    var matchingSubscriptions = Storage.GetMatchingEndpoints(dataOfInterest);
        //    foreach(var matchingSubscription in matchingSubscriptions)
        //    {
        //        // Note – Toast, Tile, Badge and Raw notification configurations are being shown 
        //        // with the same ‘notification’ variable for illustration purposes

        //        // Toast
        //        var notification = ToastContentFactory.CreateToastText01();
        //        // Detailed configuration of Toast contents omitted for brevity...

        //        // Tile 
        //        var notification = TileContentFactory.CreateTileWideText01();
        //        // Detailed configuration of Tile contents omitted for brevity...
                

        //        // Badge
        //        var notification = BadgeContentFactory.CreateBadgeNumeric();
        //        // Detailed configuration of Badge contents omitted for brevity...

        //        // Raw
        //        var notification = RawContentFactory.CreateRaw();
        //        notification.Content = "Some raw content";

        //        // Send the notification and examine the results
        //        var uri = new Uri(matchingSubscription.SubscriptionUri);
        //        // To prevent hijacking, ensure that the host contains "notify.windows.com"
        //        if (!uri.Host.ToLowerInvariant().Contains("notify.windows.com"))
        //        {
        //            // Bad URI - possible data hijack attempt.  Remove the subscription
        //            SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
        //            continue;
        //        }
        //        var result = notification.Send(uri, tokenProvider);
        //        switch (result.StatusCode)
        //        {
        //            case HttpStatusCode.Gone:
        //                // The Channel URI has expired.  Don't send to it anymore.
        //                SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
        //                break;
        //            case HttpStatusCode.NotFound:
        //                // The Channel URI is not a valid URI.  Don't send to it anymore.
        //                SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
        //                break;
        //            case HttpStatusCode.NotAcceptable:
        //                // The channel throttle limit has been exceeded.  
        //                // Stop sending Notifications for a while
        //                break;
        //            //case HttpStatusCode.Unauthorized:           
        //            //    // The access token has expired.  Renew it.
        //            // NOTE:  Not needed - handled internally by helper library.
        //            //    tokenProvider = new WnsAccessTokenProvider(CliendSID, ClientSecret);
        //            //    break;
        //        }
        //        // TODO - Based on StatusCode, etc. log issues for troubleshooting & support
        //    }
        //}

        // Use the WCF Test Client to invoke this
        public void PublishToastNotification(String zipCodeOfInterest)
        {
            var tokenProvider = new WnsAccessTokenProvider(CliendSID, ClientSecret);

            var matchingSubscriptions = SimulatedSubscriptionStorage.GetMatchingSubscriptions(zipCodeOfInterest);
            foreach(var matchingSubscription in matchingSubscriptions)
            {
                var notification = ToastContentFactory.CreateToastText01();
                notification.TextBodyWrap.Text = "Some Push Notification text";

                var uri = new Uri(matchingSubscription.SubscriptionUri);
                // To prevent data hijacking, ensure that the host contains "notify.windows.com"
                if (!uri.Host.ToLowerInvariant().Contains("notify.windows.com"))
                {
                    // Bad URI - possible data hijack attempt.  Remove the subscription
                    SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
                    continue;
                }
                var result = notification.Send(uri, tokenProvider);
                switch (result.StatusCode)
                {
                    case HttpStatusCode.Gone:
                        // The Channel URI has expired.  Don't send to it anymore.
                        SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
                        break;
                    case HttpStatusCode.NotFound:
                        // The Channel URI is not a valid URI.  Don't send to it anymore.
                        SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
                        break;
                    case HttpStatusCode.NotAcceptable:
                        // The channel throttle limit has been exceeded.  Stop sending Notifications for a while
                        break;
                    //case HttpStatusCode.Unauthorized:           
                    //    // The access token has expired.  Renew it.
                    // NOTE:  Not needed - handled internally by helper library.
                    //    tokenProvider = new WnsAccessTokenProvider(CliendSID, ClientSecret);
                    //    break;
                }
            }
        }

        public void PublishTileUpdate(String zipCodeOfInterest)
        {
            var tokenProvider = new WnsAccessTokenProvider(CliendSID, ClientSecret);

            var matchingSubscriptions = SimulatedSubscriptionStorage.GetMatchingSubscriptions(zipCodeOfInterest);
            foreach (var matchingSubscription in matchingSubscriptions)
            {
                var notification = TileContentFactory.CreateTileWideText01();
                notification.TextHeading.Text = "Some Push Notification Text";
                var squareNotification = TileContentFactory.CreateTileSquareBlock();
                squareNotification.TextBlock.Text = "Some Push Notification Text";
                notification.SquareContent = squareNotification;

                var uri = new Uri(matchingSubscription.SubscriptionUri);
                // To prevent hijacking, ensure that the host contains "notify.windows.com"
                if (!uri.Host.ToLowerInvariant().Contains("notify.windows.com"))
                {
                    // Bad URI - possible data hijack attempt.  Remove the subscription
                    SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
                    continue;
                }
                var result = notification.Send(uri, tokenProvider);
                switch (result.StatusCode)
                {
                    case HttpStatusCode.Gone:
                        // The Channel URI has expired.  Don't send to it anymore.
                        SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
                        break;
                    case HttpStatusCode.NotFound:
                        // The Channel URI is not a valid URI.  Don't send to it anymore.
                        SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
                        break;
                    case HttpStatusCode.NotAcceptable:
                        // The channel throttle limit has been exceeded.  Stop sending Notifications for a while
                        break;
                    //case HttpStatusCode.Unauthorized:           
                    //    // The access token has expired.  Renew it.
                    // NOTE:  Not needed - handled internally by helper library.
                    //    tokenProvider = new WnsAccessTokenProvider(CliendSID, ClientSecret);
                    //    break;
                }
            }
        }

        public void PublishBadgeUpdate(String zipCodeOfInterest)
        {
            var tokenProvider = new WnsAccessTokenProvider(CliendSID, ClientSecret);

            var matchingSubscriptions = SimulatedSubscriptionStorage.GetMatchingSubscriptions(zipCodeOfInterest);
            foreach (var matchingSubscription in matchingSubscriptions)
            {
                var notification = BadgeContentFactory.CreateBadgeNumeric();
                notification.Number = 42;

                var uri = new Uri(matchingSubscription.SubscriptionUri);
                // To prevent data hijacking, ensure that the host contains "notify.windows.com"
                if (!uri.Host.ToLowerInvariant().Contains("notify.windows.com"))
                {
                    // Bad URI - possible data hijack attempt.  Remove the subscription
                    SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
                    continue;
                }
                var result = notification.Send(uri, tokenProvider);
                switch (result.StatusCode)
                {
                    case HttpStatusCode.Gone:
                        // The Channel URI has expired.  Don't send to it anymore.
                        SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
                        break;
                    case HttpStatusCode.NotFound:
                        // The Channel URI is not a valid URI.  Don't send to it anymore.
                        SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
                        break;
                    case HttpStatusCode.NotAcceptable:
                        // The channel throttle limit has been exceeded.  Stop sending Notifications for a while
                        break;
                    //case HttpStatusCode.Unauthorized:           
                    //    // The access token has expired.  Renew it.
                    // NOTE:  Not needed - handled internally by helper library.
                    //    tokenProvider = new WnsAccessTokenProvider(CliendSID, ClientSecret);
                    //    break;
                }
            }
        }

        public void PublishRawNotification(String zipCodeOfInterest)
        {
            var tokenProvider = new WnsAccessTokenProvider(CliendSID, ClientSecret);

            var matchingSubscriptions = SimulatedSubscriptionStorage.GetMatchingSubscriptions(zipCodeOfInterest);
            foreach (var matchingSubscription in matchingSubscriptions)
            {
                var notification = RawContentFactory.CreateRaw();
                notification.Content = "Some raw content";

                var uri = new Uri(matchingSubscription.SubscriptionUri);
                // To prevent data hijacking, ensure that the host contains "notify.windows.com"
                if (!uri.Host.ToLowerInvariant().Contains("notify.windows.com"))
                {
                    // Bad URI - possible data hijack attempt.  Remove the subscription
                    SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
                    continue;
                }
                var result = notification.Send(uri, tokenProvider);
                switch (result.StatusCode)
                {
                    case HttpStatusCode.Gone:
                        // The Channel URI has expired.  Don't send to it anymore.
                        SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
                        break;
                    case HttpStatusCode.NotFound:
                        // The Channel URI is not a valid URI.  Don't send to it anymore.
                        SimulatedSubscriptionStorage.RemoveBadSubscription(matchingSubscription);
                        break;
                    case HttpStatusCode.NotAcceptable:
                        // The channel throttle limit has been exceeded.  Stop sending Notifications for a while
                        break;
                    //case HttpStatusCode.Unauthorized:           
                    //    // The access token has expired.  Renew it.
                    // NOTE:  Not needed - handled internally by helper library.
                    //    tokenProvider = new WnsAccessTokenProvider(CliendSID, ClientSecret);
                    //    break;
                }
            }
        }
    }
}
