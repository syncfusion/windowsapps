﻿using System;
using System.Collections.Generic;
using System.Linq;
using NotificationsExtensions.BadgeContent;
using NotificationsExtensions.TileContent;
using NotificationsExtensions.ToastContent;
using Windows.Networking.PushNotifications;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Notifications;
using Windows.UI.StartScreen;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using WindowsStoreAppsSuccinctly.Common;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void HandleRegisterChannelButtonClick(object sender, RoutedEventArgs e)
        {
            RegisterChannel();
        }

        private Boolean _isInterceptNotificationsOn;
        private async void RegisterChannel()
        {
            // Request the channel
            var channel = await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();
            //var secondaryTileChannel = await PushNotificationChannelManager.CreatePushNotificationChannelForSecondaryTileAsync("Tile Id");
            
            // Display the Channel URI in the UI
            CurrentChannelText.Text = channel.Uri;

            // Subscribe to the notification being received while the app is running
            channel.PushNotificationReceived += (o, e) =>
                                                     {
                                                         if (!_isInterceptNotificationsOn) return;

                                                         if (e.NotificationType == PushNotificationType.Raw && e.RawNotification != null)
                                                         {
                                                             // TODO - Process the supplied raw notification contents
                                                             //DoSomethingWithRawNotificationContent(e.RawNotification.Content);
                                                         }
                                                         else
                                                         {
                                                             // Illustrates cancelling Windows' default handling of the notification.
                                                             e.Cancel = true;
                                                         }
                                                         Dispatcher.RunAsync(
                                                             CoreDispatcherPriority.Normal, 
                                                             () => new Windows.UI.Popups.MessageDialog("Notification Receievd - " + e.NotificationType).ShowAsync());
                                                     };

            // Retrieve the previous published channel Uri (if any) from settings
            var oldChannelUri = String.Empty;
            var localSettings = ApplicationData.Current.LocalSettings;
            if (localSettings.Values.ContainsKey("previousChannelUri"))
            {
                var oldChannelUriValue = localSettings.Values["previousChannelUri"];
                oldChannelUri = oldChannelUriValue == null ? String.Empty : oldChannelUriValue.ToString();
            }

            // Get the zip code of interest from somewhere(location, settings, etc.)
            var zipCodeOfInterest = ZipCodeText.Text;

            var oldZipCodeOfInterest = String.Empty;
            if (localSettings.Values.ContainsKey("previousZipCode"))
            {
                var oldZipCodeValue = localSettings.Values["previousZipCode"];
                oldZipCodeOfInterest = oldZipCodeValue == null ? String.Empty : oldZipCodeValue.ToString();
            }
            var isNewChannelUri = !oldChannelUri.Equals(channel.Uri, StringComparison.CurrentCultureIgnoreCase);
            var isNewZipCodeValue = !oldZipCodeOfInterest.Equals(zipCodeOfInterest, StringComparison.CurrentCultureIgnoreCase);

            // Send an updated channel Uri to the service, if necessary
            if (isNewChannelUri || isNewZipCodeValue)
            {
                // The Channel has changed from the previous value(if any).  Publish to service
                PublishChannelToService(channel.Uri, zipCodeOfInterest);

                // Cache the sent Uri value & zip code
                localSettings.Values["previousChannelUri"] = channel.Uri;
                localSettings.Values["previousZipCode"] = zipCodeOfInterest;
            }            
        }

        private void PublishChannelToService(String uriToPublish, String zipCodeOfInterest)
        {
            // Set up and store an id for this client if one has not yet been set up
            Guid subscriberId = Guid.Empty;
            var localSettings = ApplicationData.Current.LocalSettings;
            if (localSettings.Values.ContainsKey("SubscriberId"))
            {
                var subscriberIdValue = localSettings.Values["SubscriberId"];
                if (subscriberIdValue != null)
                {
                    subscriberId = (Guid)subscriberIdValue;
                }
            }
            if (subscriberId == Guid.Empty)
            {
                subscriberId = Guid.NewGuid();
                localSettings.Values["SubscriberId"] = subscriberId;
            }

            var client = new SubscriptionServiceReference.ServiceSubscriptionClient();
            client.UpdateSubscriptionAsync(subscriberId, uriToPublish, zipCodeOfInterest);
        }

        private void HandleToggleInterceptNotifications(object sender, RoutedEventArgs e)
        {
            var toggleSwitch = (ToggleSwitch) sender;
            _isInterceptNotificationsOn = toggleSwitch.IsOn;
        }
    }
}
