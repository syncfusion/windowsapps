﻿using NotificationsExtensions.BadgeContent;
using NotificationsExtensions.TileContent;
using NotificationsExtensions.ToastContent;
using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI;
using Windows.UI.Notifications;
using Windows.UI.StartScreen;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            var appName = this.Resources["AppName"].ToString();
            var navigationArgs = navigationParameter == null ? String.Empty : navigationParameter.ToString();
            if (!String.IsNullOrWhiteSpace(navigationArgs))
            {
                appName = appName + " - " + navigationParameter.ToString();
            }
            pageTitle.Text = appName;
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        #region Tiles
                                                                                                  //X 
        private void HandleUpdateTileWithXmlButtonClick(Object sender, RoutedEventArgs e)
        {
            // Ensure updates are enabled
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            if (updater.Setting == NotificationSetting.Enabled)
            {
                // Get the default XML for the desired tile tamplate
                var tileSquareXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileSquareText01);

                // Locate the elements to be modified
                var textSquareElements = tileSquareXml.GetElementsByTagName("text").ToList();
                //var headerSquareElement = textSquareElements.First(x => x.Attributes[0].NodeValue.ToString() == "1");
                //headerSquareElement.InnerText = "Heading";
                //var textSquare01Element = textSquareElements.First(x => x.Attributes[0].NodeValue.ToString() == "2");
                //textSquare01Element.InnerText = "Subheading 1";
                //var textSquare02Element = textSquareElements.First(x => x.Attributes[0].NodeValue.ToString() == "3");
                //textSquare02Element.InnerText = "Subheading 2";
                //var textSquare03Element = textSquareElements.First(x => x.Attributes[0].NodeValue.ToString() == "4");
                //textSquare03Element.InnerText = "Subheading 3";

                // More detailed searching of Xml for specific attributes omitted for brevity...
                textSquareElements[0].InnerText = "Heading";
                textSquareElements[1].InnerText = "Subheading 1";
                textSquareElements[2].InnerText = "Subheading 2";
                textSquareElements[3].InnerText = "Subheading 3";

                // Get the default XML for the desired tile tamplate
                var tileWideXml = TileUpdateManager.GetTemplateContent(TileTemplateType.TileWideText01);

                // Locate the elements to be modified
                var textWideElements = tileWideXml.GetElementsByTagName("text").ToList();
                textWideElements[0].InnerText = "Wide Heading";
                textWideElements[1].InnerText = "Wide Subheading 1";
                textWideElements[2].InnerText = "Wide Subheading 2";
                textWideElements[3].InnerText = "Wide Subheading 3";

                // Inject the Wide XML's binding node contents into the visual element of the Square XML
                var wideBindingNode = tileWideXml.GetElementsByTagName("binding").First();
                var squareVisualNode = tileSquareXml.GetElementsByTagName("visual").First();
                var importNode = tileSquareXml.ImportNode(wideBindingNode, true);
                squareVisualNode.AppendChild(importNode);

                var notification = new TileNotification(tileSquareXml);
                notification.ExpirationTime = DateTimeOffset.Now.AddHours(2);
                updater.Update(notification);
            }
        }

        private void HandleUpdateTileButtonClick(Object sender, RoutedEventArgs e)
        {
            // Ensure updates are enabled
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            if (updater.Setting == NotificationSetting.Enabled)
            {
                // Prepare the square tile
                var tileSquareUpdate = NotificationsExtensions.TileContent.TileContentFactory.CreateTileSquareText01();
                tileSquareUpdate.TextHeading.Text = "Heading";
                tileSquareUpdate.TextBody1.Text = "Subheading 1";
                tileSquareUpdate.TextBody2.Text = "Subheading 2";
                tileSquareUpdate.TextBody3.Text = "Subheading 3";

                // Prepare the wide tile
                var tileWideUpdate = NotificationsExtensions.TileContent.TileContentFactory.CreateTileWideText01();
                tileWideUpdate.TextHeading.Text = "Wide Heading";
                tileWideUpdate.TextBody1.Text = "Wide Subheading 1";
                tileWideUpdate.TextBody2.Text = "Wide Subheading 2";
                tileWideUpdate.TextBody3.Text = "Wide Subheading 3";

                // Inject the square tile contents into the wide tile
                tileWideUpdate.SquareContent = tileSquareUpdate;

                // Send the notification
                var notification = tileWideUpdate.CreateNotification();
                notification.ExpirationTime = DateTimeOffset.Now.AddHours(2);
                updater.Update(notification);
            }
        }

        private void HandleQueueUpdatesButtonClick(Object sender, RoutedEventArgs e)
        {
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            if (updater.Setting == NotificationSetting.Enabled)
            {
                // Build up a list of tiles to be queued
                // Build the first tiles
                var tileSquareUpdate = TileContentFactory.CreateTileSquareText01();
                tileSquareUpdate.TextHeading.Text = "Queue 1";
                tileSquareUpdate.TextBody1.Text = "Subheading Q1-1";
                tileSquareUpdate.TextBody2.Text = "Subheading Q1-2";
                tileSquareUpdate.TextBody3.Text = "Subheading Q1-3";

                var tileWideUpdate = TileContentFactory.CreateTileWideText01();
                tileWideUpdate.TextHeading.Text = "Wide Queue 1";
                tileWideUpdate.TextBody1.Text = "Wide Subheading Q1-1";
                tileWideUpdate.TextBody2.Text = "Wide Subheading Q1-2";
                tileWideUpdate.TextBody3.Text = "Wide Subheading Q1-3";
                tileWideUpdate.SquareContent = tileSquareUpdate;

                updater.Update(tileWideUpdate.CreateNotification());

                // Build the second tiles
                var tileSquareUpdate2 = TileContentFactory.CreateTileSquarePeekImageAndText01();
                tileSquareUpdate2.TextHeading.Text = "Queue 2";
                tileSquareUpdate2.TextBody1.Text = "Subheading Q2-1";
                tileSquareUpdate2.TextBody2.Text = "Subheading Q2-2";
                tileSquareUpdate2.TextBody3.Text = "Subheading Q2-3";
                tileSquareUpdate2.Image.Src = "ms-appx:///Assets/Logo.png";

                var tileWideUpdate2 = TileContentFactory.CreateTileWidePeekImage02();
                tileWideUpdate2.TextHeading.Text = "Wide Queue 2";
                tileWideUpdate2.TextBody1.Text = "Wide Subheading Q2-1";
                tileWideUpdate2.TextBody2.Text = "Wide Subheading Q2-2";
                tileWideUpdate2.TextBody3.Text = "Wide Subheading Q2-3";
                tileWideUpdate2.Image.Src = "ms-appx:///Assets/LogoWide.png";
                tileWideUpdate2.SquareContent = tileSquareUpdate2;

                updater.Update(tileWideUpdate2.CreateNotification());

                // Build the third tiles
                var tileSquareUpdate3 = TileContentFactory.CreateTileSquareImage();
                tileSquareUpdate3.Image.Src = "ms-appx:///Assets/Logo.png";

                var tileWideUpdate3 = TileContentFactory.CreateTileWideImageAndText01();
                tileWideUpdate3.TextCaptionWrap.Text = "Wide Queue 3";
                tileWideUpdate3.Image.Src = "ms-appx:///Assets/LogoWide.png";
                tileWideUpdate3.SquareContent = tileSquareUpdate3;

                updater.Update(tileWideUpdate3.CreateNotification());

                // Enable queuing
                updater.EnableNotificationQueue(true);
            }
        }

        private void HandleScheduleTileUpdateButtonClick(Object sender, RoutedEventArgs e)
        {
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            if (updater.Setting == NotificationSetting.Enabled)
            {
                // Set up the tile that is to appear at a later time
                var tileSquareUpdate = TileContentFactory.CreateTileSquareText01();
                tileSquareUpdate.TextHeading.Text = "Scheduled";
                tileSquareUpdate.TextBody1.Text = "Scheduled Sub 1";
                tileSquareUpdate.TextBody2.Text = "Scheduled Sub  2";
                tileSquareUpdate.TextBody3.Text = "Scheduled Sub  3";

                var tileWideUpdate = TileContentFactory.CreateTileWideText01();
                tileWideUpdate.TextHeading.Text = "Schedueld Wide";
                tileWideUpdate.TextBody1.Text = "Wide Scheduled Sub  1";
                tileWideUpdate.TextBody2.Text = "Wide Scheduled Sub  2";
                tileWideUpdate.TextBody3.Text = "Wide Scheduled Sub  3";
                tileWideUpdate.SquareContent = tileSquareUpdate;

                // Set the time when the update needs to occur as 1 hour from now
                var deliveryTime = DateTimeOffset.Now.AddHours(1);

                // Schedule the update
                var scheduledTileNotification = new ScheduledTileNotification(tileWideUpdate.GetXml(), deliveryTime);
                scheduledTileNotification.ExpirationTime = null;
                updater.AddToSchedule(scheduledTileNotification);
            }
        }

        private void HandleClearScheduledTileUpdateButtonClick(Object sender, RoutedEventArgs e)
        {
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            // Get the list of scheduled notifications
            var scheduledUpdates = updater.GetScheduledTileNotifications();
            //// Set up a scheduled notification with an Id
            // var scheduledTileNotification = new ScheduledTileNotification(tileWideUpdate.GetXml(), deliveryTime) {Id = "SomeId"};
            //// Try to find scheduled notifications with a matching Id
            //foreach(var scheduledUpdate in scheduledUpdates.Where(x => x.Id == "SomeId"))
            foreach (var scheduledUpdate in scheduledUpdates)
            {
                updater.RemoveFromSchedule(scheduledUpdate);
            }
        }

        private void HandleStartPeriodicTileUpdateClick(Object sender, RoutedEventArgs e)
        {
            var pollingUri = new Uri("http://www.wintellect.com/tile");
            var pollingUri1 = new Uri("http://www.wintellect.com/tile");
            var pollingUri2 = new Uri("http://www.wintellect.com/tile");
            var pollingUri3 = new Uri("http://www.wintellect.com/tile");

            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            if (updater.Setting == NotificationSetting.Enabled)
            {
                // Set up for polling every 30 minutes
                updater.StartPeriodicUpdate(pollingUri, PeriodicUpdateRecurrence.HalfHour);
                
                // Delay the initial request by 1 minute
                var offsetTime = DateTimeOffset.Now.AddMinutes(1);
                updater.StartPeriodicUpdate(pollingUri, offsetTime, PeriodicUpdateRecurrence.HalfHour);

                // Provide a list of URIs to call
                var batchUris = new []{pollingUri1, pollingUri2, pollingUri3};
                // Note that Notification Queuing must be enabled
                updater.EnableNotificationQueue(true);

                updater.StartPeriodicUpdateBatch(batchUris, PeriodicUpdateRecurrence.HalfHour);
            }
        }

        private void HandleStopPeriodicTileUpdateClick(Object sender, RoutedEventArgs e)
        {
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.StopPeriodicUpdate();
        }

        private void HandleClearTileButtonClick(Object sender, RoutedEventArgs e)
        {
            var updater = TileUpdateManager.CreateTileUpdaterForApplication();
            updater.Clear();
        }

        #endregion  

        #region Badges

        private void HandleSetBadgeNumberButtonClick(Object sender, RoutedEventArgs e)
        {
            //var badgeNum = (UInt32)42;
            //// Get the default Xml for a number badge
            //var badgeXml = BadgeUpdateManager.GetTemplateContent(BadgeTemplateType.BadgeNumber);

            //// Locate the badge element
            //var element = badgeXml.GetElementsByTagName("badge").Select(x => (XmlElement)x).First();

            //// Add a value attribute to the element with the number to display
            //element.SetAttribute("value", badgeNum.ToString());

            //// Get an updater and pass it the updated xml
            //var updater = BadgeUpdateManager.CreateBadgeUpdaterForApplication();
            //updater.Update(new BadgeNotification(badgeXml));

            // Prepare a numeric notification
            var content = new BadgeNumericNotificationContent(42);

            // Get an updater and pass it the updated xml
            var updater = BadgeUpdateManager.CreateBadgeUpdaterForApplication();
            var notification = content.CreateNotification();
            //notification.ExpirationTime = DateTimeOffset.Now.AddMinutes(1);
            updater.Update(notification);
        }

        private void HandleSetBadgeGlyphButtonClick(Object sender, RoutedEventArgs e)
        {
            //// Get the default Xml for a glyph badge
            //var badgeXml = BadgeUpdateManager.GetTemplateContent(BadgeTemplateType.BadgeGlyph);

            //// Locate the badge element
            //var element = badgeXml.GetElementsByTagName("badge").Select(x => (XmlElement)x).First();

            //// Add a value attribute to the element with the number to display
            //element.SetAttribute("value", "alert");

            //// Get an updater and pass it the updated xml
            //var updater = BadgeUpdateManager.CreateBadgeUpdaterForApplication();
            //updater.Update(new BadgeNotification(badgeXml));

            // Prepare a glyph notification
            var content = new BadgeGlyphNotificationContent(GlyphValue.Away);

            // Get an updater and pass it the updated xml
            var updater = BadgeUpdateManager.CreateBadgeUpdaterForApplication();
            var notification = content.CreateNotification();
            //notification.ExpirationTime = DateTimeOffset.Now.AddMinutes(1);
            updater.Update(notification);
        }

        private void HandleStartPeriodicBadgeUpdateClick(Object sender, RoutedEventArgs e)
        {
            var updater = BadgeUpdateManager.CreateBadgeUpdaterForApplication();
            updater.StartPeriodicUpdate(new Uri("http://www.wintellect.com/badge"), DateTimeOffset.Now.AddMinutes(1), PeriodicUpdateRecurrence.HalfHour);
            //updater.StartPeriodicUpdate(new Uri("http://www.wintellect.com/badge"), PeriodicUpdateRecurrence.HalfHour);
        }

        private void HandleStopPeriodicBadgeUpdateClick(Object sender, RoutedEventArgs e)
        {
            var updater = BadgeUpdateManager.CreateBadgeUpdaterForApplication();
            updater.StopPeriodicUpdate();
        }

        private void HandleClearBadgeButtonClick(Object sender, RoutedEventArgs e)
        {
            var updater = BadgeUpdateManager.CreateBadgeUpdaterForApplication();
            updater.Clear();
        }

        #endregion

        #region Secondary Tiles

        private const String SecondaryTileId = "secondaryTile1";
        private async void HandleAddSecondaryTileButtonClick(Object sender, RoutedEventArgs e)
        {
            var secondaryTile = new SecondaryTile(
                SecondaryTileId, 
                "Secondary Tile Sample", 
                "Secondary Tile Sample Display Name",
                "Secondary Tile Activation Args",
                TileOptions.ShowNameOnLogo | TileOptions.ShowNameOnWideLogo,
                new Uri("ms-appx:///Assets/Logo.png"),
                new Uri("ms-appx:///Assets/LogoWide.png"))
                {
                    BackgroundColor = Colors.ForestGreen
                };
            await secondaryTile.RequestCreateAsync();
        }

        private void HandleSetSecondaryTileBadgeButtonClick(Object sender, RoutedEventArgs e)
        {
            // Prepare a glyph notification
            var content = new BadgeGlyphNotificationContent(GlyphValue.Playing);

            // Get an updater and pass it the updated xml
            var updater = BadgeUpdateManager.CreateBadgeUpdaterForSecondaryTile(SecondaryTileId);
            updater.Update(content.CreateNotification());
        }

        private void HandleUpdateSecondaryTileButtonClick(Object sender, RoutedEventArgs e)
        {
            var updater = TileUpdateManager.CreateTileUpdaterForSecondaryTile(SecondaryTileId);
            if (updater.Setting == NotificationSetting.Enabled)
            {
                var tileSquareUpdate = NotificationsExtensions.TileContent.TileContentFactory.CreateTileSquarePeekImageAndText01();
                tileSquareUpdate.TextHeading.Text = "Second";
                tileSquareUpdate.TextBody1.Text = "Subheading Sec1";
                tileSquareUpdate.TextBody2.Text = "Subheading Sec2";
                tileSquareUpdate.TextBody3.Text = "Subheading Sec3";
                tileSquareUpdate.Image.Src = "ms-appx:///Assets/Logo.png";

                var tileWideUpdate = NotificationsExtensions.TileContent.TileContentFactory.CreateTileWidePeekImage02();
                tileWideUpdate.TextHeading.Text = "Wide Second 1";
                tileWideUpdate.TextBody1.Text = "Wide Subheading Sec1";
                tileWideUpdate.TextBody2.Text = "Wide Subheading Sec2";
                tileWideUpdate.TextBody3.Text = "Wide Subheading Sec3";
                tileWideUpdate.TextBody4.Text = "Wide Subheading Sec3";
                tileWideUpdate.Image.Src = "ms-appx:///Assets/LogoWide.png";
                tileWideUpdate.SquareContent = tileSquareUpdate;

                updater.Update(tileWideUpdate.CreateNotification());
            }
        }

        private async void HandleRemoveSecondaryTileButtonClick(Object sender, RoutedEventArgs e)
        {
            //var matchingTileExists = Windows.UI.StartScreen.SecondaryTile.Exists(SecondaryTileId);
            //if (matchingTileExists)
            {
                //var currentSecondaryTiles = await Windows.UI.StartScreen.SecondaryTile.FindAllForPackageAsync();
                //var matchingTile = currentSecondaryTiles.FirstOrDefault(x => x.TileId == SecondaryTileId);
                var matchingTile = new SecondaryTile(SecondaryTileId);
                await matchingTile.RequestDeleteAsync();
            }
        }

        #endregion

        // TOASTS - DON'T FORGET TO TURN ON TOASTS IN THE MANIFEST!
        #region Toasts

        private void HandleShowToastButtonClick(Object sender, RoutedEventArgs e)
        {
            // Ensure updates are enabled
            var notifier = ToastNotificationManager.CreateToastNotifier();
            if (notifier.Setting == NotificationSetting.Enabled)
            {
                // Build the content of the toast notification
                var content = ToastContentFactory.CreateToastImageAndText04();
                content.TextHeading.Text = "Toast Heading";
                content.TextBody1.Text = "Toast Subheading 1";
                content.TextBody1.Text = "Toast Subheading 2";
                content.Image.Src = "ms-appx:///Assets/LogoWide.png";
                
                // Specify parameters to be passed to the App on Launch 
                content.Launch = "App Launch Params";

                // Show the notification
                var notification = content.CreateNotification();
                notifier.Show(notification);
            }
        }
        private void HandleShowLongToastButtonClick(Object sender, RoutedEventArgs e)
        {
            // Ensure updates are enabled
            var notifier = ToastNotificationManager.CreateToastNotifier();
            if (notifier.Setting == NotificationSetting.Enabled)
            {
                // Build the content of the toast notification
                var content = ToastContentFactory.CreateToastImageAndText04();
                content.TextHeading.Text = "Long Toast Heading";
                content.TextBody1.Text = "Long Toast Subheading 1";
                content.TextBody1.Text = "Long Toast Subheading 2";
                content.Image.Src = "ms-appx:///Assets/LogoWide.png";

                // Specify parameters to be passed to the App on Launch 
                content.Launch = "App Launch Params";

                // Specify long-duration and a looping sound
                content.Duration = ToastDuration.Long;
                content.Audio.Content = ToastAudioContent.LoopingCall2;
                content.Audio.Loop = true;

                // Show the notification
                var notification = content.CreateNotification();
                notifier.Show(notification);
            }
        }
        private void HandleShowScheduledToastButtonClick(Object sender, RoutedEventArgs e)
        {

            var notifier = ToastNotificationManager.CreateToastNotifier();
            if (notifier.Setting == NotificationSetting.Enabled)
            {
                // Build the content of the toast notification
                var content = ToastContentFactory.CreateToastImageAndText04();
                content.TextHeading.Text = "Scheduled Toast Heading";
                content.TextBody1.Text = "Scheduled Toast Subheading 1";
                content.TextBody1.Text = "Scheduled Toast Subheading 2";
                content.Image.Src = "ms-appx:///Assets/LogoWide.png";
                content.Launch = "App Launch Params";

                // Set the time at which the toast should be triggered
                var deliveryTime = DateTimeOffset.Now.AddMinutes(1);

                // Configure a one-time scheduled toast
                var scheduledNotification = new ScheduledToastNotification(
                                                      content.GetXml(), 
                                                      deliveryTime);

                //// Configure a repeating scheduled toast
                //var snoozeInterval = TimeSpan.FromMinutes(5);   // Must be between 60 seconds and 60 minutes
                //var maximumSnoozeCount = (UInt32)5; // Can be 1-5 times
                //var scheduledNotification = new ScheduledToastNotification(
                //                                    content.GetXml(), 
                //                                    deliveryTime, 
                //                                    snoozeInterval, 
                //                                    maximumSnoozeCount);

                notifier.AddToSchedule(scheduledNotification);
            }
        }

        private void HandleStopPendingScheduledToastButtonClick(Object sender, RoutedEventArgs e)
        {
            // Retrieve the scheduled notifications
            var notifier = ToastNotificationManager.CreateToastNotifier();
            var notifications = notifier.GetScheduledToastNotifications();

            // Alternatively, find the notifications that have a specific Id
            //var notifications = new ScheduledToastNotification(content.GetXml(), deliveryTime) {Id = "ScheduledId"};
            //var notifications = notifier.GetScheduledToastNotifications().FirstOrDefault(x => x.Id == "ScheduledId");

            // Remove the indicated notifications
            foreach (var notification in notifications)
            {
                notifier.RemoveFromSchedule(notification);
            }
        }

        private void HandleInAppToastButtonClick(Object sender, RoutedEventArgs e) 
        {
            var notifier = ToastNotificationManager.CreateToastNotifier();
            if (notifier.Setting == NotificationSetting.Enabled)
            {
                // Build the content of the toast notification
                var content = ToastContentFactory.CreateToastImageAndText04();
                content.TextHeading.Text = "Toast Heading";
                content.TextBody1.Text = "Toast Subheading 1";
                content.TextBody1.Text = "Toast Subheading 2";
                content.Image.Src = "ms-appx:///Assets/LogoWide.png";

                // Specify parameters to be passed to the App on Launch 
                content.Launch = "App Launch Params";

                // Create the notification instance
                var notification = content.CreateNotification();

                // Subscribe to the activation event
                notification.Activated += (activatedNotification, args) =>
                {
                    //activatedNotification
                };

                // Subscribe to the dismissal event
                notification.Dismissed += (dismissedNotification, args) =>
                {
                    //args.Reason == ToastDismissalReason.ApplicationHidden;
                    //args.Reason == ToastDismissalReason.TimedOut;
                    //args.Reason == ToastDismissalReason.UserCanceled;
                    //dismissedNotification
                };

                // Show the notification
                notifier.Show(notification);
            }
        }

        #endregion
    }
}
