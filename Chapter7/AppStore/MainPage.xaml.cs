﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// This #if approach allows swapping out to the "real" app when deploying to the store.
// Note that this sample is geared around the idea of loading app information via pre-canned XML, 
// which is not part of the post-ship Windows Store experience.
#if DEBUG
    using CurrentAppAccessor = Windows.ApplicationModel.Store.CurrentAppSimulator;
#else
    using CurrentAppAccessor = Windows.ApplicationModel.Store.CurrentApp;
#endif

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();
            UpdateOptionsControls();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private void HandleLoadFullAppProfileClick(Object sender, RoutedEventArgs e)
        {
            // Load a proxy file that simulates a full-feature app
            LoadProxyFile("WindowsStoreProxy_FullApp.xml");
        }

        private void HandleLoadTrialAppProfileClick(Object sender, RoutedEventArgs e)
        {
            // Load a proxy file that simulates a trial-mode app
            LoadProxyFile("WindowsStoreProxy_TrialApp.xml");
        }

        private void HandleLoadInAppPurchaseProfileClick(Object sender, RoutedEventArgs e)
        {
            // Load a proxy file that simulates an app that supports in-app purchases
            LoadProxyFile("WindowsStoreProxy_InAppPurchase.xml");
        }

        private async void HandleUpgradeTrialAppClick(Object sender, RoutedEventArgs e)
        {
            // Request a purchase upgrade from the Trial Mode to the Full Feature Mode
            try
            {
                var receiptText = await CurrentAppAccessor.RequestAppPurchaseAsync(true);
                // In practice, the resuls in these exceptions can be of interest.
            }
            catch (System.Runtime.InteropServices.COMException)
            {
                // Happens with E_FAIL
            }
            catch (ArgumentException)
            {
                // Happens with E_INVALIDARG result
            }
            catch (OutOfMemoryException)
            {
                // Happens with E_OUTOFMEMORY result
            }
        }

        private async void HandlePurchaseProductClick(Object sender, RoutedEventArgs e)
        {
            var selectedItem = AvailableProductsList.SelectedItem as Windows.ApplicationModel.Store.ProductListing;
            if (selectedItem != null)
            {
                var productId = selectedItem.ProductId;

                try
                {
                    // Request an in-app pruchase of a specific product
                    var receiptText = await CurrentAppAccessor.RequestProductPurchaseAsync(productId, true);
                    // In practice, the resuls in these exceptions can be of interest.
                }
                catch (System.Runtime.InteropServices.COMException)
                {
                    // Happens with E_FAIL
                }
                catch (ArgumentException)
                {
                    // Happens with E_INVALIDARG result
                }
                catch (OutOfMemoryException)
                {
                    // Happens with E_OUTOFMEMORY result
                }
            }
        }

        private async void LoadProxyFile(String proxyProfileFileToLoad)
        {
            // Initialize WindowsStoreProxy.xml
            // Create the fixed folder for proxy files
            //var folder = await Windows.Storage.ApplicationData.Current.LocalFolder.CreateFolderAsync("Microsoft\\Windows Store\\ApiData", Windows.Storage.CreationCollisionOption.OpenIfExists);

            // Load the 
            //var file = await Package.Current.InstalledLocation.GetFileAsync("WindowsStoreProxy.xml");
            //var storeProxyFile = await folder.CreateFileAsync(proxyProfileFileToLoad, Windows.Storage.CreationCollisionOption.ReplaceExisting);
            //await baselineStoreProxyFile.CopyAndReplaceAsync(storeProxyFile);
            
            // Load the proxy XML file to use
            var baselineStoreProxyFile = await Windows.Storage.StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///" + proxyProfileFileToLoad));
            // Update the simulator to take its values from those defined in the XML file
            await CurrentAppAccessor.ReloadSimulatorAsync(baselineStoreProxyFile);

            // Update the UI to show the current app licensing status
            UpdateStatusControls();
            UpdateOptionsControls();
        }

        private Windows.ApplicationModel.Store.LicenseInformation _licenseInformation;
        private Windows.ApplicationModel.Store.ListingInformation _listingInformation;

        private async void HandleLicenseInformationLicenseChanged()
        {
            // When the current license is changed (through an app store purchase or trial upgrade) update the UI accordingly
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                UpdateStatusControls();
                UpdateOptionsControls();
            });
        }

        private async void UpdateStatusControls()
        {
            AppIdText.Text = CurrentAppAccessor.AppId.ToString();
            LinkText.Text = CurrentAppAccessor.LinkUri.ToString();

            // Information that describes the application's listing in the Store
            _listingInformation = await CurrentAppAccessor.LoadListingInformationAsync();
            AppNameText.Text = _listingInformation.Name;
            AppDescriptionText.Text = _listingInformation.Description;
            AgeRatingText.Text = _listingInformation.AgeRating.ToString();
            MarketText.Text = _listingInformation.CurrentMarket;
            PriceText.Text = _listingInformation.FormattedPrice;
            // TODO - replace the simple count display with either an items control or a combo box to show the actual items...
            AvailableProductsText.Text = _listingInformation.ProductListings.Count().ToString();

            // Information about the application's current licensing status
            if (_licenseInformation != null) _licenseInformation.LicenseChanged -= HandleLicenseInformationLicenseChanged;
            _licenseInformation = CurrentAppAccessor.LicenseInformation;
            if (_licenseInformation != null) _licenseInformation.LicenseChanged += HandleLicenseInformationLicenseChanged;
            LicenseActiveText.Text = _licenseInformation.IsActive.ToString();
            LicenseTrialText.Text = _licenseInformation.IsTrial.ToString();
            LicenseExpirationText.Text = _licenseInformation.ExpirationDate.ToString("G");
            // TODO - replace the simple count display with either an items control or a combo box to show the actual items...
            LicenseProductsText.Text = _licenseInformation.ProductLicenses.Count().ToString();

            // Information that describes purchases made inthe application so far
            var appReceipt = await CurrentAppAccessor.GetAppReceiptAsync();
            //var productReceipt = await Windows.ApplicationModel.Store.CurrentAppSimulator.GetProductReceiptAsync(productId);

            // Set the Ad Control visibility based on the current Demo Mode
            AdControl.Visibility = _licenseInformation.IsTrial ? Visibility.Visible : Visibility.Collapsed;
        }

        private void UpdateOptionsControls()
        {
            // Update the status of the "upgrade trial" button
            UpgradeTrialButton.IsEnabled = _licenseInformation != null && _licenseInformation.IsTrial;

            // Popuplate the in-app-purchase combo box with the list of available products that can be purchased
            AvailableProductsList.Items.Clear();
            if (_listingInformation != null && _listingInformation != null)
            {
                var availableListingItems = _listingInformation.ProductListings
                                            .Where(x => _licenseInformation.ProductLicenses.ContainsKey(x.Value.ProductId) == false)
                                            .ToList();
                foreach (var listingItem in availableListingItems)
                {
                    AvailableProductsList.Items.Add(listingItem.Value);
                }
                AvailableProductsList.DisplayMemberPath = "Name";
            }

            // Select the first item in the list by default 
            if (AvailableProductsList.Items.Any())
            {
                AvailableProductsList.IsEnabled = true;
                AvailableProductsList.SelectedItem = AvailableProductsList.Items[0];
            }
            else
            {
                AvailableProductsList.IsEnabled = false;
            }

            // Update the status of the "purchase product" button
            PurchaseProductButton.IsEnabled = AvailableProductsList.SelectedItem != null;
        }
    }
}
