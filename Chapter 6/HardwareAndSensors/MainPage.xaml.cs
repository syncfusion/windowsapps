﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237
namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();

            InitializeLocation();
            InitializeSensors();
            InitializeMultimedia();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        #region Location/Position

        // The Locator used for position information
        private Windows.Devices.Geolocation.Geolocator _locator = null;
        private const Double DefaultMovementThresholdMeters = 20.0;
        private const UInt32 DefaultMinimumReportIntervalMilliseconds = 3000;

        private void InitializeLocation()
        {
            var locator = new Windows.Devices.Geolocation.Geolocator();
            var locationStatus = locator.LocationStatus;
            if (locationStatus != Windows.Devices.Geolocation.PositionStatus.Disabled)
            {
                // Only proceed if the system is not set to disable Location
                _locator = locator;

                // Subscribe to the Locator's status chage event
                _locator.StatusChanged += HandleLocatorStatusChanged;

                // Set minimum threshold in meters
                _locator.MovementThreshold = DefaultMovementThresholdMeters;

                // Set minimum interval between updates in milliseconds
                _locator.ReportInterval = DefaultMinimumReportIntervalMilliseconds;

                // Subscribe to the position changed event
                _locator.PositionChanged += HandleLocatorPositionChanged;
            }

            EnableLocationUIControls(locationStatus);
        }

        private void CloseLocator()
        {
            if (_locator == null) return;

            _locator.StatusChanged -= HandleLocatorStatusChanged;
            _locator.PositionChanged -= HandleLocatorPositionChanged;
            _locator = null;
        }

        private async void HandleLocatorStatusChanged(Windows.Devices.Geolocation.Geolocator sender, Windows.Devices.Geolocation.StatusChangedEventArgs args)
        {
            // Marshall the call to update the UI using the Dispatcher
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                () =>
                {
                    var locatorStatus = args.Status;
                    EnableLocationUIControls(locatorStatus);
                    if (locatorStatus == Windows.Devices.Geolocation.PositionStatus.Disabled)
                    {
                        // The control is disabled - clear the display of captured location information and throw away the Locator;
                        ClearLocationDisplay();
                        CloseLocator();
                    }
                });
        }

        private void EnableLocationUIControls(Windows.Devices.Geolocation.PositionStatus status)
        {
            // Enable/disable event toggle UI control based on the Locator's readiness
            Boolean isReady = status == Windows.Devices.Geolocation.PositionStatus.Ready;
            LocationEnableUpdateEventsToggle.IsEnabled = isReady;
        }

        private async void HandleLocationUpdateNowClicked(Object sender, RoutedEventArgs e)
        {
            // If the locator was removed because location was disabled, see if it has been re-enabled by trying to reinitialize the locator
            if (_locator == null)
            {
                InitializeLocation();
            }

            // If the locator was re-enabled and it is "ready", get the current position
            if (_locator != null && _locator.LocationStatus == Windows.Devices.Geolocation.PositionStatus.Ready)
            {
                try
                {
                    // Obtain the position information
                    var position = await _locator.GetGeopositionAsync();
                    UpdateLocationDisplay(position);
                }
                catch (UnauthorizedAccessException)
                {
                    // Something went wrong - there is a chance the locator is disabled.  Reset the locator
                    CloseLocator();
                    InitializeLocation();
                }
            }
        }

        private void HandleLocationUpdateEventsChanged(Object sender, RoutedEventArgs e)
        {
            // If the state is currently "disabled", do nothing
            if (_locator == null) return;

            // Check if the Locator is "ready" and the toggle is turned to the on position
            if (LocationEnableUpdateEventsToggle.IsOn)
            {
                _locator.PositionChanged += HandleLocatorPositionChanged;
            }
            else
            {
                // Unsubscribe to the position changed event
                _locator.PositionChanged -= HandleLocatorPositionChanged;
            }
        }

        private async void HandleLocatorPositionChanged(Windows.Devices.Geolocation.Geolocator sender, Windows.Devices.Geolocation.PositionChangedEventArgs args)
        {
            // Marshall the call to update the UI using the Dispatcher
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                () =>
                {
                    UpdateLocationDisplay(args.Position);
                });
        }

        private void UpdateLocationDisplay(Windows.Devices.Geolocation.Geoposition position)
        {
            // Populate text controls based on the position values (taking into account ones that may be unavailable)
            LatitudeTextBox.Text = position.Coordinate.Latitude.ToString();
            LongitudeTextBox.Text = position.Coordinate.Longitude.ToString();
            AltitudeTextBox.Text = position.Coordinate.Altitude.GetValueOrDefault().ToString();

            AccuracyTextBox.Text = position.Coordinate.Accuracy.ToString();
            AltitudeAcuraccyTextBox.Text = position.Coordinate.AltitudeAccuracy.GetValueOrDefault().ToString();

            SpeedTextBox.Text = position.Coordinate.Speed.GetValueOrDefault().ToString();
            HeadingTextBox.Text = position.Coordinate.Heading.GetValueOrDefault().ToString();

            UpdatedAtTextBox.Text = DateTimeOffset.Now.ToString("T");
        }

        private void ClearLocationDisplay()
        {
            // Clear the user's location values (usually for privacy when the locator is disabled)
            LatitudeTextBox.Text = String.Empty;
            LongitudeTextBox.Text = String.Empty;
            AltitudeTextBox.Text = String.Empty;

            AccuracyTextBox.Text = String.Empty;
            AltitudeAcuraccyTextBox.Text = String.Empty;

            SpeedTextBox.Text = String.Empty;
            HeadingTextBox.Text = String.Empty;

            UpdatedAtTextBox.Text = DateTimeOffset.Now.ToString("T");
        }

        #endregion

        #region Motion/Orientation

        private Windows.Devices.Sensors.SimpleOrientationSensor _simpleOrientationSensor;
        private Windows.Devices.Sensors.Accelerometer _accelerometer;
        private Windows.Devices.Sensors.Compass _compass;
        private Windows.Devices.Sensors.Gyrometer _gyrometer;
        private Windows.Devices.Sensors.Inclinometer _inclinometer;
        private Windows.Devices.Sensors.OrientationSensor _orientationSensor;

        private Windows.Devices.Sensors.LightSensor _lightSensor;

        private void InitializeSensors()
        {
            // Will be null if no such sensor is available
            _simpleOrientationSensor = Windows.Devices.Sensors.SimpleOrientationSensor.GetDefault();

            _accelerometer = Windows.Devices.Sensors.Accelerometer.GetDefault();
            _compass = Windows.Devices.Sensors.Compass.GetDefault();
            _gyrometer = Windows.Devices.Sensors.Gyrometer.GetDefault();
            _inclinometer = Windows.Devices.Sensors.Inclinometer.GetDefault();
            _orientationSensor = Windows.Devices.Sensors.OrientationSensor.GetDefault();

            _lightSensor = Windows.Devices.Sensors.LightSensor.GetDefault();

            if (_simpleOrientationSensor != null)
            {
                _simpleOrientationSensor.OrientationChanged += HandleSimpleOrientationSensorReadingChanged;
            }

            if (_accelerometer != null)
            {
                // milliseconds, zero = use driver default, change sensitivity (G-force) is updated based on report interval (longer interval, higher change sensitivity)
                //_accelerometer.ReportInterval = Math.Max(_accelerometer.MinimumReportInterval, desiredAccelerometerReportInterval);
                _accelerometer.ReadingChanged += HandleAccelerometerReadingChanged;
                _accelerometer.Shaken += HandleAccelerometerShaken;
            }

            if (_gyrometer != null)
            {
                // milliseconds, zero = use driver default, sensitivity is updated based on report interval (longer interval, higher change sensitivity)
                //_gyrometer.ReportInterval = Math.Max(_gyrometer.MinimumReportInterval, desiredGyrometerReportInterval);
                _gyrometer.ReadingChanged += HandleGyrometerReadingChanged;
            }

            if (_inclinometer != null)
            {
                // milliseconds, zero = use driver default, change sensitivity (degrees) is updated based on report interval (longer interval, higher change sensitivity)
                //_inclinometer.ReportInterval = Math.Max(_inclinometer.MinimumReportInterval, desiredInclinometerReportInterval);
                _inclinometer.ReadingChanged += HandleInclinometerReadingChanged;
            }

            if (_orientationSensor != null)
            {
                // milliseconds, zero = use driver default, change sensitivity (degrees) is updated based on report interval (longer interval, higher change sensitivity)
                //_orientationSensor.ReportInterval = Math.Max(_orientationSensor.MinimumReportInterval, desiredOrientationReportInterval);
                _orientationSensor.ReadingChanged += HandleOrientationReadingChanged;
            }

            if (_lightSensor != null)
            {
                // milliseconds, zero = use driver default, change sensitivity (percentage) is updated based on report interval (longer interval, higher change sensitivity)
                //_lightSensor.ReportInterval = Math.Max(_lightSensor.MinimumReportInterval, desiredLightSensorReportInterval);
                _lightSensor.ReadingChanged += HandleLightSensorReadingChanged;
            }
        }

        private void HandleSensorUpdateButtonClick(Object sender, RoutedEventArgs e)
        {
            if (_simpleOrientationSensor != null)
            {
                var simpleOrientationReading = _simpleOrientationSensor.GetCurrentOrientation();
                DisplaySimpleOrientationReading(simpleOrientationReading, DateTimeOffset.Now);
            }

            if (_accelerometer != null)
            {
                var accelerometerReading = _accelerometer.GetCurrentReading();
                DisplayAccelerometerReading(accelerometerReading); //Accel X/Y/Z (units?)
            }

            if (_compass != null)
            {
                var compassReading = _compass.GetCurrentReading();
                DisplayCompassReading(compassReading); //HeadingMag, HeadingTrue(nullable) (units?)
            }

            if (_gyrometer != null)
            {
                var gyrometerReading = _gyrometer.GetCurrentReading();
                DisplayGyrometerReading(gyrometerReading); //AngularVelX, AngularVelY, AngularVelZ
            }

            if (_inclinometer != null)
            {
                var inclinometerReading = _inclinometer.GetCurrentReading();
                DisplayInclinometerReading(inclinometerReading); // Pitch, Roll, Yaw (degrees)
            }

            if (_orientationSensor != null)
            {
                var orientationReading = _orientationSensor.GetCurrentReading();
                DisplayOrientationReading(orientationReading); // Quaternion, Rotation Matrix
            }

            if (_lightSensor != null)
            {
                var lightReading = _lightSensor.GetCurrentReading();
                DisplayLightSensorReading(lightReading);   //Illuminance in Lux
            }
        }

        private async void HandleSimpleOrientationSensorReadingChanged(Windows.Devices.Sensors.SimpleOrientationSensor sender, Windows.Devices.Sensors.SimpleOrientationSensorOrientationChangedEventArgs args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (SensorUpdateEventsToggle.IsOn)
                {
                    DisplaySimpleOrientationReading(args.Orientation, args.Timestamp);
                }
            });
        }

        private async void HandleAccelerometerReadingChanged(Windows.Devices.Sensors.Accelerometer sender, Windows.Devices.Sensors.AccelerometerReadingChangedEventArgs args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (SensorUpdateEventsToggle.IsOn)
                {
                    DisplayAccelerometerReading(args.Reading);
                }
            });
        }

        private async void HandleAccelerometerShaken(Windows.Devices.Sensors.Accelerometer sender, Windows.Devices.Sensors.AccelerometerShakenEventArgs args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (SensorUpdateEventsToggle.IsOn)
                {
                    DisplayAccelerometerShaken(args.Timestamp);
                }
            });
        }

        private async void HandleGyrometerReadingChanged(Windows.Devices.Sensors.Gyrometer sender, Windows.Devices.Sensors.GyrometerReadingChangedEventArgs args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (SensorUpdateEventsToggle.IsOn)
                {
                    DisplayGyrometerReading(args.Reading);
                }
            });
        }

        private async void HandleInclinometerReadingChanged(Windows.Devices.Sensors.Inclinometer sender, Windows.Devices.Sensors.InclinometerReadingChangedEventArgs args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (SensorUpdateEventsToggle.IsOn)
                {
                    DisplayInclinometerReading(args.Reading);
                }
            });
        }

        private async void HandleOrientationReadingChanged(Windows.Devices.Sensors.OrientationSensor sender, Windows.Devices.Sensors.OrientationSensorReadingChangedEventArgs args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (SensorUpdateEventsToggle.IsOn)
                {
                    DisplayOrientationReading(args.Reading);
                }
            });
        }

        private async void HandleLightSensorReadingChanged(Windows.Devices.Sensors.LightSensor sender, Windows.Devices.Sensors.LightSensorReadingChangedEventArgs args)
        {
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                if (SensorUpdateEventsToggle.IsOn)
                {
                    DisplayLightSensorReading(args.Reading);
                }
            });
        }

        private void DisplaySimpleOrientationReading(Windows.Devices.Sensors.SimpleOrientation orientation, DateTimeOffset timeStamp)
        {
            SimpleOrientationText.Text = String.Format("{0} - {1:T}", orientation, timeStamp);
        }
        
        private void DisplayAccelerometerReading(Windows.Devices.Sensors.AccelerometerReading accelerometerReading)
        {
            AccelerometerText.Text = String.Format("X: {0}, Y: {1}, Z: {2} - {3:T}", accelerometerReading.AccelerationX, accelerometerReading.AccelerationY, accelerometerReading.AccelerationZ, accelerometerReading.Timestamp);
        }

        private void DisplayAccelerometerShaken(DateTimeOffset timestamp)
        {
            AccelerometerShakeText.Text = String.Format("Shake detected - {0:T}", timestamp);
        }

        private void DisplayCompassReading(Windows.Devices.Sensors.CompassReading compassReading)
        {
            CompassText.Text = String.Format("Mag N - {0}, True N - {1} - {2:T}", compassReading.HeadingMagneticNorth, compassReading.HeadingTrueNorth, compassReading.Timestamp);
        }

        private void DisplayGyrometerReading(Windows.Devices.Sensors.GyrometerReading gyrometerReading)
        {
            GyrometerText.Text = String.Format("VX: {0}, VY: {1}, VZ: {2} - {3:T}", gyrometerReading.AngularVelocityX, gyrometerReading.AngularVelocityY, gyrometerReading.AngularVelocityZ, gyrometerReading.Timestamp);
        }

        private void DisplayInclinometerReading(Windows.Devices.Sensors.InclinometerReading inclinometerReading)
        {
            InclinationText.Text = String.Format("Pitch: {0}, Roll: {1}, Yaw: {2} - {3:T}", inclinometerReading.PitchDegrees, inclinometerReading.RollDegrees, inclinometerReading.YawDegrees, inclinometerReading.Timestamp);
        }

        private void DisplayOrientationReading(Windows.Devices.Sensors.OrientationSensorReading orientationReading)
        {
            OrientationText.Text = String.Format("Quaternion, Rotation Matrix - {0:T}", orientationReading.Timestamp);
        }

        private void DisplayLightSensorReading(Windows.Devices.Sensors.LightSensorReading lightReading)
        {
            LightSensorText.Text = String.Format("Luminance: {0} - {1:T}", lightReading.IlluminanceInLux, lightReading.Timestamp);
        }

        #endregion

        #region Multimedia

        private void InitializeMultimedia()
        {
            EnumerateCameras();
            EnumerateMicrophones();
            EnableMultimediaControls();

            Application.Current.Resuming += (o, e) =>
                {
                    ResetPassthrough();
                };

            Application.Current.Suspending += async (o, e) =>
                {
                    if (_captureManager == null) return;

                    var deferral = e.SuspendingOperation.GetDeferral();
                    await _captureManager.StopPreviewAsync();
                    _captureManager = null;
                    deferral.Complete();
                };
        }

        private async void HandleMultimediaStartSimpleCaptureButtonClick(Object sender, RoutedEventArgs e)
        {
            var cameraCaptureUI = new Windows.Media.Capture.CameraCaptureUI();
            var videoSettings = cameraCaptureUI.VideoSettings;
            //videoSettings.AllowTrimming
            //videoSettings.Format = Windows.Media.Capture.CameraCaptureUIVideoFormat.Wmv; // Mp4
            //videoSettings.MaxDurationInSeconds = 120;
            //videoSettings.MaxResolution = Windows.Media.Capture.CameraCaptureUIMaxVideoResolution.HighestAvailable; //.StandardDefinition
            var cameraSettings = cameraCaptureUI.PhotoSettings;
            //cameraSettings.AllowCropping
            //cameraSettings.CroppedAspectRatio = new Size(width, height);
            //cameraSettings.CroppedSizeInPixels = new Size(pixelWidth, pixelHeight);
            //cameraSettings.Format = Windows.Media.Capture.CameraCaptureUIPhotoFormat.Png; //.Jpeg
            //cameraSettings.MaxResolution = Windows.Media.Capture.CameraCaptureUIMaxPhotoResolution.HighestAvailable; //.Large3M

            var capturedFile = await cameraCaptureUI.CaptureFileAsync(Windows.Media.Capture.CameraCaptureUIMode.PhotoOrVideo);
            if (capturedFile != null)
            {
                var savePicker = new Windows.Storage.Pickers.FileSavePicker();

                // Figure out the start folder based on whether image or video content was returned
                if (capturedFile.ContentType.StartsWith("image", StringComparison.OrdinalIgnoreCase))
                {
                    savePicker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
                }
                else
                {
                    savePicker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.VideosLibrary;
                }

                savePicker.FileTypeChoices.Add(capturedFile.DisplayType, new List<String> { capturedFile.FileType });
                savePicker.SuggestedFileName = capturedFile.Name;
                var saveFile = await savePicker.PickSaveFileAsync();
                if (saveFile != null)
                {
                    await capturedFile.CopyAndReplaceAsync(saveFile);
                }
            }

            ResetPassthrough();
        }

        private async void EnumerateCameras()
        {
            // Identify the available video devices and show them in the UI
            var videoDevices = await Windows.Devices.Enumeration.DeviceInformation.FindAllAsync(Windows.Devices.Enumeration.DeviceClass.VideoCapture);

            CameraList.DisplayMemberPath = "Name";
            CameraList.ItemsSource = videoDevices;
        }

        private void HandleCameraListSelectionChanged(Object sender, SelectionChangedEventArgs e)
        {
            ResetPassthrough();
            EnableMultimediaControls();
        }

        private async void EnumerateMicrophones()
        {
            // Identify the available audio devices and show them in the UI
            var audioDevices = await Windows.Devices.Enumeration.DeviceInformation.FindAllAsync(Windows.Devices.Enumeration.DeviceClass.AudioCapture);

            MicrophoneList.DisplayMemberPath = "Name";
            MicrophoneList.ItemsSource = audioDevices;
        }

        private void HandleMicrophoneListSelectionChanged(Object sender, SelectionChangedEventArgs e)
        {
            ResetPassthrough();
            EnableMultimediaControls();
        }

        private void EnableMultimediaControls()
        {
            var isVideoDeviceSelected = CameraList.SelectedItem != null;
            var isAudioDeviceSelected = MicrophoneList.SelectedItem != null;
            //MultimediaStartSimpleCaptureButton.IsEnabled = isVideoDeviceSelected;

            MultimediaSettingsButton.IsEnabled = isVideoDeviceSelected;
            MultimediaCaptureButton.IsEnabled = isVideoDeviceSelected || isAudioDeviceSelected;
        }

        private Windows.Media.Capture.MediaCapture _captureManager;

        private async void ResetPassthrough()
        {
            var captureMode = Windows.Media.Capture.StreamingCaptureMode.AudioAndVideo;

            var selectedVideoDevice = (Windows.Devices.Enumeration.DeviceInformation)CameraList.SelectedItem;
            if (selectedVideoDevice == null) return;

            var selectedAudioDevice = (Windows.Devices.Enumeration.DeviceInformation)MicrophoneList.SelectedItem;
            if (selectedVideoDevice == null && selectedAudioDevice == null) return;
            if (selectedVideoDevice != null && selectedAudioDevice == null) captureMode = Windows.Media.Capture.StreamingCaptureMode.Video;
            if (selectedVideoDevice == null && selectedAudioDevice != null) captureMode = Windows.Media.Capture.StreamingCaptureMode.Audio;

            var settings = new Windows.Media.Capture.MediaCaptureInitializationSettings
                {
                    StreamingCaptureMode = captureMode,
                    VideoDeviceId = selectedVideoDevice == null ? String.Empty : selectedVideoDevice.Id,
                    AudioDeviceId = selectedAudioDevice == null ? String.Empty : selectedAudioDevice.Id,
                };
            _captureManager = new Windows.Media.Capture.MediaCapture();
            await _captureManager.InitializeAsync(settings);

            VideoCapturePreviewRegion.Source = _captureManager;
            await _captureManager.StartPreviewAsync();
        }

        private void HandleMultimediaSettings(Object sender, RoutedEventArgs e)
        {
            // Show a media settings UI screen for the Media Capture Manager
            Windows.Media.Capture.CameraOptionsUI.Show(_captureManager); 
        }

        private async void HandleCaptureClick(Object sender, RoutedEventArgs e)
        {
            var popupMenu = new Windows.UI.Popups.PopupMenu();

            if (!String.IsNullOrWhiteSpace(_captureManager.MediaCaptureSettings.VideoDeviceId))
            {
                popupMenu.Commands.Add(new Windows.UI.Popups.UICommand("Capture Picture", HandleMultimediaCapturePhotoClick));
                popupMenu.Commands.Add(new Windows.UI.Popups.UICommand("Capture Video", HandleMultimediaCaptureVideoClick));
            }
            if (!String.IsNullOrWhiteSpace(_captureManager.MediaCaptureSettings.AudioDeviceId))
            {
                popupMenu.Commands.Add(new Windows.UI.Popups.UICommand("Capture Audio", HandleMultimediaCaptureAudioClick));
            }

            if (popupMenu.Commands.Any())
            {
                var senderButton = (Button)sender;
                var transform = senderButton.TransformToVisual(this);
                var point = transform.TransformPoint(new Point(0, 0));
                await popupMenu.ShowAsync(point);
            }
        }

        private async void HandleMultimediaCapturePhotoClick(Windows.UI.Popups.IUICommand command)
        {
            // Configure the File Save Picker and get the file to save into
            var savePicker = new Windows.Storage.Pickers.FileSavePicker
            {
                SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary,
                SuggestedFileName = "Webcam Picture"
            };
            savePicker.FileTypeChoices.Add("PNG File (PNG)", new List<String> { ".png" });

            var saveFile = await savePicker.PickSaveFileAsync();
            if (saveFile != null)
            {
                // Set the properties for the picture to capture
                var pictureProperties = new Windows.Media.MediaProperties.ImageEncodingProperties
                {
                    Width = 320,
                    Height = 240,
                    Subtype = "PNG",
                };

                // Write the picture into the file
                await _captureManager.CapturePhotoToStorageFileAsync(pictureProperties, saveFile);
            }
        }

        private async void HandleMultimediaCaptureVideoClick(Windows.UI.Popups.IUICommand command)
        {            
            // Configure and get the file to save into
            var savePicker = new Windows.Storage.Pickers.FileSavePicker
            {
                SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.VideosLibrary,
                SuggestedFileName = "Direct Video"
            };

            savePicker.FileTypeChoices.Add("WMV File", new List<String> { ".wmv" });

            var saveFile = await savePicker.PickSaveFileAsync();
            if (saveFile != null)
            {
                // Set the media encoding profile properties
                var desiredEncodingQuality = Windows.Media.MediaProperties.VideoEncodingQuality.Ntsc;
                var encodingProfile = Windows.Media.MediaProperties.MediaEncodingProfile.CreateWmv(desiredEncodingQuality);

                // Provide feedback
                MultimediaCaptureStatusText.Text = "Capturing Video " + saveFile.Name;
                
                // Start capture
                await _captureManager.StartRecordToStorageFileAsync(encodingProfile, saveFile);
                
                // Wait 5 seconds
                await System.Threading.Tasks.Task.Delay(TimeSpan.FromSeconds(5));

                // Stop capture
                await _captureManager.StopRecordAsync();
                MultimediaCaptureStatusText.Text = "Capture complete";
            }
        }

        private async void HandleMultimediaCaptureAudioClick(Windows.UI.Popups.IUICommand command)
        {
            // Configure and get the file to save into
            var savePicker = new Windows.Storage.Pickers.FileSavePicker
            {
                SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.MusicLibrary,
                SuggestedFileName = "Direct Audio"
            };

            savePicker.FileTypeChoices.Add("WMA File", new List<String> { ".wma" });

            var saveFile = await savePicker.PickSaveFileAsync();
            if (saveFile != null)
            {
                // Set the media encoding profile properties
                var encodingProfile = Windows.Media.MediaProperties.MediaEncodingProfile.CreateMp3(Windows.Media.MediaProperties.AudioEncodingQuality.Medium);

                // Set up a timer to provide feedback
                MultimediaCaptureStatusText.Text = "Capturing Audio " + saveFile.Name;

                // Start capture
                await _captureManager.StartRecordToStorageFileAsync(encodingProfile, saveFile);

                // Wait 5 seconds
                await System.Threading.Tasks.Task.Delay(TimeSpan.FromSeconds(5));

                // Stop capture
                await _captureManager.StopRecordAsync();
                MultimediaCaptureStatusText.Text = "Capture complete";
            }
        }
        #endregion
    }
}