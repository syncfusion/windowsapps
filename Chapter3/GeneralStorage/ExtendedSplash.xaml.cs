﻿using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ExtendedSplash : Page
    {
        private readonly SplashScreen _splash;

        public ExtendedSplash()
        {
            this.InitializeComponent();
        }

        public ExtendedSplash(SplashScreen splashScreen)
            : this()
        {
            _splash = splashScreen;
            Window.Current.SizeChanged += (o, e) =>
            {
                // This will be executed in response to snapping, unsnapping, rotation, etc...
                UpdateSplashContentPositions();
            };

            // Set the initial position(s)
            UpdateSplashContentPositions();
        }

        private void UpdateSplashContentPositions()
        {
            if (_splash == null) return;
            var splashImageRect = _splash.ImageLocation;
            ExtendedSplashImage.SetValue(Canvas.LeftProperty, splashImageRect.X);
            ExtendedSplashImage.SetValue(Canvas.TopProperty, splashImageRect.Y);
            ExtendedSplashImage.Height = splashImageRect.Height;
            ExtendedSplashImage.Width = splashImageRect.Width;

            // Position the extended splash screen's progress ring.
            var progressRingTop = splashImageRect.Y + splashImageRect.Height + 20;
            var progressRingLeft = splashImageRect.X + (splashImageRect.Width / 2) - ProgressRing.Width / 2;
            ProgressRing.SetValue(Canvas.TopProperty, progressRingTop);
            ProgressRing.SetValue(Canvas.LeftProperty, progressRingLeft);
        }
    }
}
