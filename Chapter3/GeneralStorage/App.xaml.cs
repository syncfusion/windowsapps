﻿using System;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using WindowsStoreAppsSuccinctly.Common;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            DoLongStartup(args.PreviousExecutionState);

            if (Window.Current.Content == null)
            {
                Window.Current.Content = new ExtendedSplash(args.SplashScreen);
            }
            Window.Current.Activate();
        }

            
            //Frame rootFrame = Window.Current.Content as Frame;

            //// Do not repeat app initialization when the Window already has content,
            //// just ensure that the window is active
            //if (rootFrame == null)
            //{
            //    // Create a Frame to act as the navigation context and navigate to the first page
            //    rootFrame = new Frame();

            //    if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
            //    {
            //        //TODO: Load state from previously suspended application
            //    }

            //    // Place the frame in the current Window
            //    Window.Current.Content = rootFrame;
            //}

            //if (rootFrame.Content == null)
            //{
            //    // When the navigation stack isn't restored navigate to the first page,
            //    // configuring the new page by passing required information as a navigation
            //    // parameter
            //    if (!rootFrame.Navigate(typeof(MainPage), args.Arguments))
            //    {
            //        throw new Exception("Failed to create initial page");
            //    }
            //}
            //// Ensure the current window is active
            //Window.Current.Activate();
        //}

        private async void DoLongStartup(ApplicationExecutionState previousExecutionState)
        {
            // Simulate the long-running task
            //await Task.Delay(10000);

            AccessLocalSettings();
            AccessLocalFolderStorage();
            AccessRoamingAppData();
            UpdateVersion();

            // On completion, navigate to the real main landing page
            // TODO: Register the Windows.ApplicationModel.Search.SearchPane.GetForCurrentView().QuerySubmitted
            // event in OnWindowCreated to speed up searches once the application is already running

            // If the Window isn't already using Frame navigation, insert our own Frame
            var previousContent = Window.Current.Content;
            var frame = previousContent as Frame;

            // If the app does not contain a top-level frame, it is possible that this 
            // is the initial launch of the app. Typically this method and OnLaunched 
            // in App.xaml.cs can call a common method.
            if (frame == null)
            {
                // Create a Frame to act as the navigation context and associate it with
                // a SuspensionManager key
                frame = new Frame();
                WindowsStoreAppsSuccinctly.Common.SuspensionManager.RegisterFrame(frame, "AppFrame");

                if (previousExecutionState == ApplicationExecutionState.Terminated)
                {
                    // Restore the saved session state only when appropriate
                    try
                    {
                        await WindowsStoreAppsSuccinctly.Common.SuspensionManager.RestoreAsync();
                    }
                    catch (WindowsStoreAppsSuccinctly.Common.SuspensionManagerException)
                    {
                        //Something went wrong restoring state.
                        //Assume there is no state and continue
                    }
                }
                Window.Current.Content = frame;
            }

            if (frame.Content == null)
            {
                frame.Navigate(typeof(MainPage));
            }
       }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspending(Object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            await SuspensionManager.SaveAsync();
            deferral.Complete();
        }

        protected override void OnActivated(IActivatedEventArgs args)
        {
            base.OnActivated(args);
        }

        public void AccessLocalSettings()
        {
            var localSettingsStore = ApplicationData.Current.LocalSettings;

            // Simple Settings Values
            localSettingsStore.Values["TextValue"] = "Value 1";
            localSettingsStore.Values["IntValue"] = 42;

            // Composite Value
            var compositeValue = new ApplicationDataCompositeValue();
            compositeValue["CompositeValue1"] = "Composite 1";
            compositeValue["CompositeValue2"] = 42;
            localSettingsStore.Values["CompositeValue"] = compositeValue;

            // Using a custom container - data in
            var createdContainer = localSettingsStore.CreateContainer("TestContainer", ApplicationDataCreateDisposition.Always);
            createdContainer.Values["SomeOtherValue"] = "Value 2";

            // Using a custom container - data out
            var accessedContainer = localSettingsStore.Containers["TestContainer"];
            var value = accessedContainer.Values["SomeOtherValue"];
        }

        public async void AccessLocalFolderStorage()
        {
            var localFolderStore = ApplicationData.Current.LocalFolder;

            // Create or open a file in LocalFolder and write some data into it
            var writeFile = await localFolderStore.CreateFileAsync("SampleFile.dat", 
                                                        CreationCollisionOption.ReplaceExisting);
            await FileIO.WriteTextAsync(writeFile, "Some Text");

            // Open the file from LocalFolder and read the text out of it
            var readFile = await localFolderStore.GetFileAsync("SampleFile.dat");
            var text = await FileIO.ReadTextAsync(readFile);
        }

        public void AccessRoamingAppData()
        {
            // Obtain the remaining storage space
            UInt64 quota = ApplicationData.Current.RoamingStorageQuota;
            
            // Subscribe to the data change event to be notified when data is synchronized.
            ApplicationData.Current.DataChanged += (o, e) =>
            {
                // Update the application based on the possibility of new data
            };

            // Write to the "high Priority" value
            var roamingStore = ApplicationData.Current.RoamingSettings;
            roamingStore.Values["HighPriority"] = "Some high priority value";
        }

        public async void UpdateVersion()
        {
            UInt32 newVersion = 2;
            await ApplicationData.Current.SetVersionAsync(newVersion,
                (setVersionRequest) =>
                {
                    var currentVersion = setVersionRequest.CurrentVersion;
                    var desiredVersion = setVersionRequest.DesiredVersion;
                    // Implement custom logic to access data and promote it from current to desired version
                });

            var updatedVersion = ApplicationData.Current.Version;
        }

        public async void ClearStores()
        {
            // Clear just the Local store
            await ApplicationData.Current.ClearAsync(ApplicationDataLocality.Local);

            // Clear all stores
            await ApplicationData.Current.ClearAsync();
        }

        public static void DoSomething()
        {
            ApplicationData.Current.SignalDataChanged();
        }
    }

    public class PLMAwareStopwatch
    {
        public PLMAwareStopwatch()
        {
        }

        public PLMAwareStopwatch(DateTimeOffset startTime, TimeSpan startOffset)
        {
            StartTime = startTime;
            StartOffset = startOffset;
        }

        public void Start()
        {
            StartOffset = new TimeSpan();
            StartTime = DateTimeOffset.Now;
        }

        public void Pause()
        {
            StartOffset = ElapsedTime;
            IsPaused = true;
        }

        public void Unpause()
        {
            StartTime = DateTimeOffset.Now;
            IsPaused = false;
        }

        public Boolean IsPaused
        {
            get;
            private set;
        }

        public DateTimeOffset StartTime
        {
            get;
            private set;
        }

        public TimeSpan StartOffset
        {
            get;
            private set;
        }

        public TimeSpan ElapsedTime
        {
            get
            {
                return DateTimeOffset.Now
                            .Subtract(StartTime)
                            .Add(StartOffset);
            }
        }
    }
}
