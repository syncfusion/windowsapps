﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using WindowsStoreAppsSuccinctly.Common;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        #region File Pickers

        private async void HandleShowFileSavePickerClicked(object sender, RoutedEventArgs e)
        {
            // The app must first be unsnapped before showing the pickers
            if (!EnsureUnsnapped()) return;

            var savePicker = new FileSavePicker
            {
                SuggestedStartLocation = PickerLocationId.DocumentsLibrary,
                DefaultFileExtension = ".txt",
                SuggestedFileName = "Some File Name",
                CommitButtonText = "Save",
            };
            savePicker.FileTypeChoices.Add("Plain Text", new List<String>() { ".txt", ".text" });

            var chosenFile = await savePicker.PickSaveFileAsync();
            if (chosenFile != null)
            {
                new Windows.UI.Popups.MessageDialog("The file " + chosenFile.Name + " was selected.").ShowAsync();
            }
        }

        private async void HandleShowFileOpenPickerClicked(object sender, RoutedEventArgs e)
        {
            // The app must first be unsnapped before showing the pickers
            if (!EnsureUnsnapped()) return;

            var openPicker = new FileOpenPicker
            {
                //SuggestedStartLocation = PickerLocationId.DocumentsLibrary,
                CommitButtonText = "Open",
                ViewMode = PickerViewMode.List,
            };
            openPicker.FileTypeFilter.Add(".txt");
            openPicker.FileTypeFilter.Add(".text");

            var chosenFile = await openPicker.PickSingleFileAsync();
            if (chosenFile != null)
            {
                new Windows.UI.Popups.MessageDialog("The file " + chosenFile.Name + " was selected.").ShowAsync();
            }
        }

        private async void HandleShowFolderPickerClicked(object sender, RoutedEventArgs e)
        {
            // The app must first be unsnapped before showing the pickers
            if (!EnsureUnsnapped()) return;

            var folderPicker = new FolderPicker
            {
                CommitButtonText = "Select",
                SuggestedStartLocation = PickerLocationId.DocumentsLibrary,
                ViewMode = PickerViewMode.Thumbnail,
            };
            // Use "." to just show folders and not display any files
            folderPicker.FileTypeFilter.Add(".");

            var chosenFolder = await folderPicker.PickSingleFolderAsync();
            if (chosenFolder != null)
            {
                new Windows.UI.Popups.MessageDialog("The folder " + chosenFolder.Name + " was selected.").ShowAsync();
            }
        }

        private Boolean EnsureUnsnapped()
        {
            // FilePicker APIs will not work if the application is in a snapped state.
            var unsnapped = ApplicationView.Value != ApplicationViewState.Snapped;
            if (!unsnapped) unsnapped = ApplicationView.TryUnsnap();
            if (!unsnapped)
            {
                new Windows.UI.Popups.MessageDialog("The app must be unsnapped to show the Pickers.").ShowAsync();
            }
            return unsnapped;
        }
        
        #endregion

        #region Programmatic IO

        private async void HandleOpenImageProgrammaticallyClicked(object sender, RoutedEventArgs e)
        {
            // Locate the well-known folder, assuming the capability has been declared
            var storageFolder = KnownFolders.PicturesLibrary;
            if (storageFolder == null) return;

            // Locate a subfolder
            StorageFolder succinctlyFolder;
            try
            {
                succinctlyFolder = await storageFolder.GetFolderAsync("Succinctly");
            }
            catch (FileNotFoundException)
            {
                new Windows.UI.Popups.MessageDialog("The folder could not be found.").ShowAsync();
                return;
            }

            // Locate a specific file
            StorageFile succinctlyImageFile;
            try
            {
                succinctlyImageFile = await succinctlyFolder.GetFileAsync("Succinctly.jpg");
            }
            catch (FileNotFoundException)
            {
                new Windows.UI.Popups.MessageDialog("The file could not be found.").ShowAsync();
                return;
            }

            // If the picture was found, load it into the on-page image control
            using (var fileStream = await succinctlyImageFile.OpenAsync(FileAccessMode.Read))
            {
                var bitmapImage = new BitmapImage();
                bitmapImage.SetSource(fileStream);
                SelectedImage.Source = bitmapImage;
            }
        } 
        #endregion

        #region Save and Load Page State

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)" /> when this page was initially requested.</param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
            // Retrieve the stored value(s) and reset the control's state
            if (pageState != null && pageState.ContainsKey("StateTextBox"))
            {
                var text = pageState["StateTextBox"].ToString();
                StateTextBox.Text = text;
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState" />.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
            // Save the control's state
            pageState["StateTextBox"] = StateTextBox.Text;
        } 

        #endregion
    }
}
