﻿using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.DataTransfer;
using Windows.ApplicationModel.DataTransfer.ShareTarget;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Text;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Imaging;

// The Share Target Contract item template is documented at http://go.microsoft.com/fwlink/?LinkId=234241

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// This page allows other applications to share content through this application.
    /// </summary>
    public sealed partial class ShareTargetPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        /// <summary>
        /// Provides a channel to communicate with Windows about the sharing operation.
        /// </summary>
        private Windows.ApplicationModel.DataTransfer.ShareTarget.ShareOperation _shareOperation;

        public ShareTargetPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when another application wants to share content through this application.
        /// </summary>
        /// <param name="args">Activation data used to coordinate the process with Windows.</param>
        public async void Activate(ShareTargetActivatedEventArgs args)
        {
            this._shareOperation = args.ShareOperation;

            // Communicate metadata about the shared content through the view model
            var shareProperties = this._shareOperation.Data.Properties;
            var thumbnailImage = new BitmapImage();
            this.DefaultViewModel["Title"] = shareProperties.Title;
            this.DefaultViewModel["Description"] = shareProperties.Description;
            this.DefaultViewModel["Image"] = thumbnailImage;
            this.DefaultViewModel["Sharing"] = false;
            this.DefaultViewModel["ShowImage"] = false;
            this.DefaultViewModel["Comment"] = String.Empty;
            this.DefaultViewModel["SupportsComment"] = true;

            // Check to see if Share was invoked with a QuickLink
            if (!String.IsNullOrWhiteSpace(args.ShareOperation.QuickLinkId))
            {
                // Retrieve meaningful settings values
                var settingsId = "QuickLink" + args.ShareOperation.QuickLinkId;
                var settings = ApplicationData.Current.LocalSettings.Values[settingsId];

                // Set up the UI with the share data and the value(s) obtained from settings
                // ...
            }
            else
            {
                // Just set up the UI with the share data
                // ...
            }

            DisplayData(args);


            Window.Current.Content = this;
            Window.Current.Activate();

            //this._shareOperation.Data.
            
            // Update the shared content's thumbnail image in the background
            if (shareProperties.Thumbnail != null)
            {
                var stream = await shareProperties.Thumbnail.OpenReadAsync();
                thumbnailImage.SetSource(stream);
                this.DefaultViewModel["ShowImage"] = true;
            }

            var data = this._shareOperation.Data;
        }

        private async void DisplayData(ShareTargetActivatedEventArgs shareTargetActivatedEventArgs)
        {
            var dataPackageView = shareTargetActivatedEventArgs.ShareOperation.Data;
            
            // Retrieve Shared Text
            if (dataPackageView.Contains(StandardDataFormats.Text))
            {
                ShareTextBlock.Text = await dataPackageView.GetTextAsync();
            }

            // Retrieve Shared RTF Content
            if (dataPackageView.Contains(StandardDataFormats.Rtf))
            {
                RTFEditBox.Document.SetText(TextSetOptions.FormatRtf, await dataPackageView.GetRtfAsync());
            }

            // Retrieve Shared URI Data
            if (dataPackageView.Contains(StandardDataFormats.Uri))
            {
                var uri = await dataPackageView.GetUriAsync();
                UriTextBox.Text = uri.AbsoluteUri;
            }

            // Retrieve Shared HTML Data
            if (dataPackageView.Contains(StandardDataFormats.Html))
            {
                var sharedHtml = await dataPackageView.GetHtmlFormatAsync();
                ShareWebView.NavigateToString(sharedHtml);
                // Note - also retrieve dataPackageView.GetResourceMapAsync() and 
                // update the elements in the HTML to the value in the map entry
            }

            // Retrieve Shared Bitmap Data
            if (dataPackageView.Contains(StandardDataFormats.Bitmap))
            {
                var bitmapImage = new BitmapImage();
                var streamRef = await dataPackageView.GetBitmapAsync();
                var imageStream = await streamRef.OpenReadAsync();
                bitmapImage.SetSource(imageStream);
                ShareImage.Source = bitmapImage;
            }

            // Retrieve Shared Storage Items
            if (dataPackageView.Contains(StandardDataFormats.StorageItems))
            {
                var storageItems = await dataPackageView.GetStorageItemsAsync();
                foreach(var storageItem in storageItems)
                {
                    ShareListBox.Items.Add(storageItem.Name + " + " + storageItem.Path);
                }
            }

            // Retrieve Shared Custom Items
            if (dataPackageView.Contains("Custom Format 1"))
            {
                var customItem = await dataPackageView.GetDataAsync("Custom Format 1");
                CustomTextBlock.Text = "Custom Format 1: " + customItem.ToString();
            }

            // Retrieve Shared Callback Items
            if (dataPackageView.Contains("Custom Format 2"))
            {
                var customCallbackItem = await dataPackageView.GetDataAsync("Custom Format 2");
                CustomTextCallbackBlock.Text = "Custom Format 2: " + customCallbackItem.ToString();
            }
        }

        private async void ProcessDataExample(ShareTargetActivatedEventArgs shareTargetActivatedEventArgs)
        {
            var dataPackageView = shareTargetActivatedEventArgs.ShareOperation.Data;

            // Retrieve Shared Custom Items
            if (dataPackageView.Contains("Custom Format 1"))
            {
                var customItem = await dataPackageView.GetDataAsync("Custom Format 1");
                // TODO - make use of the custom data format value
                return;
            }

            // Retrieve Shared RTF Content
            if (dataPackageView.Contains(StandardDataFormats.Rtf))
            {
                var rtfText = await dataPackageView.GetRtfAsync();
                // TODO - make use of the shared text
                return;
            }

            // Retrieve Shared Text
            if (dataPackageView.Contains(StandardDataFormats.Text))
            {
                var sharedText = await dataPackageView.GetTextAsync();
                // TODO - make use of the shared text
                return;
            }
        }

        /// <summary>
        /// Invoked when the user clicks the Share button.
        /// </summary>
        /// <param name="sender">Instance of Button used to initiate sharing.</param>
        /// <param name="e">Event data describing how the button was clicked.</param>
        private async void ShareButton_Click(Object sender, RoutedEventArgs e)
        {
            //this.DefaultViewModel["Sharing"] = true;
            try
            {
                this._shareOperation.ReportStarted();
                // Simulate long delay
                await Task.Delay(2500);
                //throw new InvalidOperationException("Test Me");

                // Configure the QuickLink
                var bitmapUri = new Uri("ms-appx:///Assets/Logo.png");
                var thumbnail = RandomAccessStreamReference.CreateFromUri(bitmapUri);
                var quickLink = new QuickLink()
                {
                    Title = "QuickLink to Succinctly App",
                    Thumbnail = thumbnail,
                    Id = "UI/user provided data to be reused"
                };
                // Indicate the Data Formats and File Types this QuickLink will support
                quickLink.SupportedDataFormats.Add(StandardDataFormats.Text);
                quickLink.SupportedDataFormats.Add("Custom Format 1");
                //quickLink.SupportedFileTypes.Add(...);

                // Indicate that the Share Contract has been completed
                this._shareOperation.ReportCompleted(quickLink);
            }
            catch (Exception ex)
            {
                var message = "An error has occurred during sharing: " + ex.Message;
                this._shareOperation.ReportError(message);
            }
        }

        // TODO: Perform work appropriate to your sharing scenario using
        //       this._shareOperation.Data, typically with additional information captured
        //       through custom user interface elements added to this page such as 
        //       this.DefaultViewModel["Comment"]
    }
}
