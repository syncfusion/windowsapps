﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Search;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage.Streams;
using Windows.ApplicationModel.DataTransfer;
using Windows.Storage;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
            this.Resuming += App_Resuming;
        }

        void App_Resuming(object sender, object e)
        {
            
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if (!rootFrame.Navigate(typeof(MainPage), args.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }
            // Ensure the current window is active
            Window.Current.Activate();
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }

        protected override void OnWindowCreated(WindowCreatedEventArgs args)
        {
            // Obtain a reference to the DataTransferManager
            var dataTransferManager = Windows.ApplicationModel.DataTransfer.DataTransferManager.GetForCurrentView();
            
            // Subscribe to the DataRequested Event
            dataTransferManager.DataRequested += OnShareDataRequested;

            // Subscribe to the Target Application Chosen event
            // Used to get the name of the app that requested the share, mostly for analystics/statistical purposes
            dataTransferManager.TargetApplicationChosen += (sender, targetApplicationChosenEventArgs) => 
                {
                    String selectedApplicationName = targetApplicationChosenEventArgs.ApplicationName;
                };

            base.OnWindowCreated(args);
        }

        // Note - The DataRequested handler has 200 ms to complete
        // http://msdn.microsoft.com/en-us/library/windows/apps/xaml/windows.applicationmodel.datatransfer.datarequest.getdeferral.aspx

        // Show the Share panel programmatically
        //Windows.ApplicationModel.DataTransfer.DataTransferManager.ShowShareUI();
        //dataRequest.FailWithDisplayText("Text to display to say the app cannot currently Share");
        private async void OnShareDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            var dataRequest = args.Request;

            // This condition would evaluate to true if the app were in a position to not share anything.
            if (false)
            {
                var message =  "Nothing is currently selected.  Please select an item to share";
                dataRequest.FailWithDisplayText(message);
                return;
            }

            // Title and Description are required
            dataRequest.Data.Properties.Title = "Windows Store Apps Succinctly Share";
            dataRequest.Data.Properties.Description =
                "Content shared from the Windows Store Apps Succinctly book.";

            // This bitmap is used later
            var bitmapUri = new Uri("ms-appx:///Assets/Logo.png");
            var bitmapReference = RandomAccessStreamReference.CreateFromUri(bitmapUri);

            // Plain Text
            dataRequest.Data.SetText("Plain Text to share");

            // RTF Text
            dataRequest.Data.SetRtf(@"{\rtf1\ansi\pard This is some {\b bold} text.\par}");

            // A URI
            dataRequest.Data.SetUri(new Uri("http://www.syncfusion.com"));

            // HTML
            var rawHtml = "Windows Store Apps Succinctly: <img src='assets/logo.png'>";
            var formattedHtml = HtmlFormatHelper.CreateHtmlFormat(rawHtml);
            dataRequest.Data.SetHtmlFormat(formattedHtml);
            dataRequest.Data.ResourceMap.Add("assets\\logo.png", bitmapReference);

            // Share a stream that contains a bitmap's contents
            dataRequest.Data.Properties.Thumbnail = bitmapReference;
            dataRequest.Data.SetBitmap(bitmapReference);

            // Allows defining a custom type of data to exchange beyond the system provided values
            dataRequest.Data.SetData("Custom Format 1", new[] { "Some text", "other text" });

            // Allows defining an object that will only be fetched if it is explicitly requested
            dataRequest.Data.SetDataProvider("Custom Format 2", DelayedDataRequestCallback);

            // Share StorageFile items
            // In case obtaining data to be shared is an async operation, use a deferral to prevent
            // the DataRequested event from slipping through before the async operation completes.
            var deferral = dataRequest.GetDeferral();

            var file1 = await StorageFile.GetFileFromApplicationUriAsync(bitmapUri);
            dataRequest.Data.SetStorageItems(new[] { file1 }, true);

            deferral.Complete();
        }

        // An alternative to the custom type, this approach witholds the actual payload until the 
        // Share Target requests the specific format Id, at which time the callback provided 
        // is run in the Share Source app to provide the requested data.

        private async void DelayedDataRequestCallback(DataProviderRequest request)
        {
            //var requestedFormatId = request.FormatId;
            var deferral = request.GetDeferral();
            //var data = await PerformSomeAsyncOperationToGetData();
            request.SetData("This is some sample data");
            deferral.Complete();
        }

        private void OnShareTargetApplicationChosen(DataTransferManager sender, TargetApplicationChosenEventArgs args)
        {
            // Used to get the name of the app that requested the share, mostly for analystics/statistical purposes
            // args.ApplicationName
        }

        /// <summary>
        /// Invoked when the application is activated as the target of a sharing operation.
        /// </summary>
        /// <param name="args">Details about the activation request.</param>
        protected override void OnShareTargetActivated(Windows.ApplicationModel.Activation.ShareTargetActivatedEventArgs args)
        {
            //var shareOperation = args.ShareOperation;
            //shareOperation.Data.Properties.
            //var dataPackage = shareOperation.Data;
            //shareOperation.ReportCompleted();// optionally include quicklink
            //var selectedQuickLinkId = shareOperation.QuickLinkId;
            //shareOperation.ReportDataRetrieved();
            //shareOperation.ReportError("Something bad happened.");
            //shareOperation.ReportStarted();
            //shareOperation.ReportSubmittedBackgroundTask();

            var shareTargetPage = new WindowsStoreAppsSuccinctly.ShareTargetPage();
            shareTargetPage.Activate(args);
        }
    }
}
