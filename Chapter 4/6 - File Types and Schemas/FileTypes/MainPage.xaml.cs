﻿using System;
using System.Collections.Generic;
using Windows.Storage;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private async void HandleFileActivation(Object sender, RoutedEventArgs e)
        {
            // Create a dummy file with the desired extension
            var localFolder = ApplicationData.Current.LocalFolder;
            var storageFile = await localFolder.CreateFileAsync("test.succinctly", CreationCollisionOption.GenerateUniqueName);

            // Launch the file, forcing the application selection dialog to be displayed
            await Launcher.LaunchFileAsync(storageFile, new LauncherOptions { DisplayApplicationPicker = true });
        }

        private async void HandleProtocolActivation(Object sender, RoutedEventArgs e)
        {
            // Create a dummy uri with the desired protocol
            var uri = new Uri("succinctly://TestCustomProtocol");

            // Launch the uri, forcing the application selection dialog to be displayed
            await Launcher.LaunchUriAsync(uri, new LauncherOptions { DisplayApplicationPicker = true });
        }
    }
}
