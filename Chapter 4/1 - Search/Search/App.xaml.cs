﻿using System;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Search;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if (!rootFrame.Navigate(typeof(MainPage), args.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }
            // Ensure the current window is active
            Window.Current.Activate();
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }

        protected override void OnWindowCreated(WindowCreatedEventArgs args)
        {
            // Retrieve the SearchPane reference
            var searchPane = Windows.ApplicationModel.Search.SearchPane.GetForCurrentView();

            // Event raised when the text in the search pane is updated
            searchPane.QueryChanged += OnSearchQueryChanged;

            // Event raised when the search is submitted
            searchPane.QuerySubmitted += OnSearchQuerySubmitted;

            searchPane.SuggestionsRequested += OnSearchSuggestionsRequested;
            searchPane.ResultSuggestionChosen += OnSearchResultsSuggestionChosen;

            searchPane.SearchHistoryEnabled = false;

            base.OnWindowCreated(args);
        }

        private void OnSearchSuggestionsRequested(SearchPane sender, SearchPaneSuggestionsRequestedEventArgs args)
        {
            // Typically use the args.QueryText value to determine what selections to display
            var queryText = args.QueryText;

            // Request a deferral to deal with Async operations
            var deferral = args.Request.GetDeferral();
            
            // Cancel the request
            // args.Request.IsCanceled = true;

            // Append a single query suggestion item
            var querySuggestions = new [] { "First Suggestion", "Second Suggestion" };//FindQuerySuggestions(queryText).Take(2);
            args.Request.SearchSuggestionCollection.AppendQuerySuggestions(querySuggestions);

            // Add just a single query suggestion
            // args.Request.SearchSuggestionCollection.AppendQuerySuggestion(suggestion);

            // Insert a separator to distinguish between query and result suggestions
            args.Request.SearchSuggestionCollection.AppendSearchSeparator("Label for Separator");

            // Add result suggestions
            args.Request.SearchSuggestionCollection.AppendResultSuggestion(
                "Suggestion 1",
                "Matching record 1",
                "Tag1",
                RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/query.png")),
                "Alt1");

            args.Request.SearchSuggestionCollection.AppendResultSuggestion(
                "Suggestion 2",
                "Matching record 2",
                "Tag2",
                RandomAccessStreamReference.CreateFromUri(new Uri("ms-appx:///Assets/query.png")),
                "Alt2");

            deferral.Complete();
        }

        private void OnSearchResultsSuggestionChosen(SearchPane sender, SearchPaneResultSuggestionChosenEventArgs args)
        {
            var selectedItemTag = args.Tag;

            new Windows.UI.Popups.MessageDialog("The item with tag " + selectedItemTag + " was selected.").ShowAsync();

            // Given the tag, find the item that matches that tag
            //var matchingItem = FindResultSuggestionItemByTag(selectedItemTag);

            // Do something to display the specific matching item
            //DisplayMatchingItem(matchingItem);
        }

        /// <summary>
        /// Invoked when the application is activated to display search results.
        /// </summary>
        /// <param name="args">Details about the activation request.</param>
        protected async override void OnSearchActivated(Windows.ApplicationModel.Activation.SearchActivatedEventArgs args)
        {
            // TODO: Register the Windows.ApplicationModel.Search.SearchPane.GetForCurrentView().QuerySubmitted
            // event in OnWindowCreated to speed up searches once the application is already running

            // If the Window isn't already using Frame navigation, insert our own Frame
            var previousContent = Window.Current.Content;
            var frame = previousContent as Frame;

            // If the app does not contain a top-level frame, it is possible that this 
            // is the initial launch of the app. Typically this method and OnLaunched 
            // in App.xaml.cs can call a common method.
            if (frame == null)
            {
                // Create a Frame to act as the navigation context and associate it with
                // a SuspensionManager key
                frame = new Frame();
                WindowsStoreAppsSuccinctly.Common.SuspensionManager.RegisterFrame(frame, "AppFrame");

                if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    // Restore the saved session state only when appropriate
                    try
                    {
                        await WindowsStoreAppsSuccinctly.Common.SuspensionManager.RestoreAsync();
                    }
                    catch (WindowsStoreAppsSuccinctly.Common.SuspensionManagerException)
                    {
                        //Something went wrong restoring state.
                        //Assume there is no state and continue
                    }
                }
            }

            frame.Navigate(typeof(SearchResultsPage), args.QueryText);
            Window.Current.Content = frame;

            // Ensure the current window is active
            Window.Current.Activate();
        }

        /// <summary>
        /// Called when [search query submitted].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="SearchPaneQuerySubmittedEventArgs"/> instance containing the event data.</param>
        private void OnSearchQuerySubmitted(SearchPane sender, SearchPaneQuerySubmittedEventArgs args)
        {
            System.Diagnostics.Debug.WriteLine("Search submitted " + args.QueryText);

            var frame = Window.Current.Content as Frame;
            if (frame != null)
            {
                frame.Navigate(typeof(SearchResultsPage), args.QueryText);
            }
        }

        /// <summary>
        /// Called when [search query changed].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="SearchPaneQueryChangedEventArgs"/> instance containing the event data.</param>
        private void OnSearchQueryChanged(SearchPane sender, SearchPaneQueryChangedEventArgs args)
        {
            System.Diagnostics.Debug.WriteLine("Search submitted " + args.QueryText);
        }

    }
}
