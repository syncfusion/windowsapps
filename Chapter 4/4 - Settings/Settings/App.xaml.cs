﻿using Callisto.Controls;
using System;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.ApplicationSettings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
            this.Resuming += App_Resuming;
        }

        void App_Resuming(object sender, object e)
        {
            
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    //TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if (!rootFrame.Navigate(typeof(MainPage), args.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }
            // Ensure the current window is active
            Window.Current.Activate();
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }

        protected override void OnWindowCreated(WindowCreatedEventArgs args)
        {
            var settingsPane = SettingsPane.GetForCurrentView();
            settingsPane.CommandsRequested += (o, e) =>
            {
                // Create "about" and "preferences" links in the Settings Pane
                var aboutCommand = new SettingsCommand("aboutCmdId", "About", handler =>
                {
                    // Display an "About" flyout
                    var flyout = new SettingsFlyout
                    {
                        Content = new AboutControl(),
                        FlyoutWidth = SettingsFlyout.SettingsFlyoutWidth.Narrow, //Wide
                        IsOpen = true,
                    };
                });

                var optionsCommand = new SettingsCommand("optionsCmdId", "Options", handler => 
                {
                    // Display an "About" flyout
                    var flyout = new SettingsFlyout
                    {
                        Content = new OptionsControl(),
                        FlyoutWidth = SettingsFlyout.SettingsFlyoutWidth.Narrow, //Wide
                        IsOpen = true,
                    };
                });

                var settingsPaneCommandsRequest = e.Request;
                settingsPaneCommandsRequest.ApplicationCommands.Add(aboutCommand);
                settingsPaneCommandsRequest.ApplicationCommands.Add(optionsCommand);
            };

            base.OnWindowCreated(args);
        }

        //// Programmatically show the Settings Pane
        //SettingsPane.Show();

        //// Determine what screen edge the Settings pane is being shown on (SettingsEdgeLocation.Left or SettingsEdgeLocation.Right)
        //var settingsEdgeLocation = SettingsPane.Edge;
    }
}
