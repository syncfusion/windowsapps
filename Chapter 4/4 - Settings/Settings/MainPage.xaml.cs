﻿using System;
using System.Collections.Generic;
using Windows.ApplicationModel.DataTransfer;
using Windows.UI.ApplicationSettings;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        //private void HandleShareSourceButtonClick(object sender, RoutedEventArgs e)
        //{
        //    // Does nothing if not
        //    EnsureUnsnapped();
        //    DataTransferManager.ShowShareUI();
        //}

        private void HandleSettingsButtonClick(object sender, RoutedEventArgs e)
        {
            // Crash if not
            EnsureUnsnapped();
            SettingsPane.Show();
        }

        public Boolean EnsureUnsnapped()
        {
            // Several APIs will not work if the application is in a snapped state.
            var unsnapped = ApplicationView.Value != ApplicationViewState.Snapped;
            if (!unsnapped) unsnapped = ApplicationView.TryUnsnap();
            if (!unsnapped)
            {
                // TODO - Notify the user that the picker cannot be displayed since the
                // application cannot be unsnapped
            }
            return unsnapped;
        }
    }
}
