﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Windows.Graphics.Printing;
using Windows.Graphics.Printing.OptionDetails;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Printing;
using WindowsStoreAppsSuccinctly.Common;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : LayoutAwarePage
    {
        // Set the print document.  HAS TO BE CREATED ON THE UI THREAD!!!!
        private PrintDocument _printDocument;

        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // Create the PrintDocument and subscribe to its events
            _printDocument = new PrintDocument();
            _printDocument.Paginate += OnRequestPrintPreviewPages;
            _printDocument.GetPreviewPage += OnGetPreviewPage;
            _printDocument.AddPages += OnAddPrintPages;

            // If this handler is declared, printers are shown when the Device Charm is
            // invoked.  Put another way, when there is a handler, there is an expectation that
            // printing is currently possible.
            // Likewise, if this handler is subscribed to twice, an exception is thrown
            var printManager = Windows.Graphics.Printing.PrintManager.GetForCurrentView();
            printManager.PrintTaskRequested += OnPrintTaskRequested;
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            // Unsubscribe when leaving the page
            var printManager = Windows.Graphics.Printing.PrintManager.GetForCurrentView();
            printManager.PrintTaskRequested -= OnPrintTaskRequested;
        }

        private void OnPrintTaskRequested(PrintManager sender,
                                    PrintTaskRequestedEventArgs args)
        {
            var printTask = args.Request.CreatePrintTask("Print task title",
            async taskSourceRequestedArgs =>
            {
                await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal,
                    () =>
                    {
                        // Called when a printer device is selected
                        taskSourceRequestedArgs.SetSource(_printDocument.DocumentSource);
                    });
            });

            // Called before pages are requested for preview
            printTask.Previewing += (s, e) => Debug.WriteLine("Submitting");

            // Called when the user hits "print"
            printTask.Submitting += (s, e) => Debug.WriteLine("Submitting");

            // Called once for each page that is printed
            printTask.Progressing += (s, e) => Debug.WriteLine("Progressing - " +
                                            e.DocumentPageCount);

            // Called after all pages are printed
            printTask.Completed += (s, e) =>
            {
                // Completion values include Abandoned, Canceled, Failed, Submitted 
                if (e.Completion == PrintTaskCompletion.Failed)
                {
                    //Notify user that an error occurred - needs to be marshalled to UI thread.
                }
            };

            SetupPrintOptions(printTask);
        }

        private void SetupPrintOptions(PrintTask printTask)
        {
            // Get an OptionDetails item from the options on the current print task
            var advancedPrintOptions = PrintTaskOptionDetails.GetFromPrintTaskOptions(printTask.Options);

            // Choose which of the "standard" printer options should be shown.
            // The order in which the options are appended determines the order in which they appear in the UI
            var displayedOptions = advancedPrintOptions.DisplayedOptions;
            displayedOptions.Clear();

            displayedOptions.Add(StandardPrintTaskOptions.Copies);
            displayedOptions.Add(StandardPrintTaskOptions.Orientation);
            //displayedOptions.Add(StandardPrintTaskOptions.ColorMode);

            // Create and populate a new custom option element - shown as a list option UI element
            var colorIdOption = advancedPrintOptions.CreateItemListOption("ColorId", "Color");
            colorIdOption.AddItem("RedId", "Red");
            colorIdOption.AddItem("GreenId", "Green");
            colorIdOption.AddItem("BlueId", "Blue");
            colorIdOption.TrySetValue("RedId");

            // Add the custom option item to the UI
            displayedOptions.Add("ColorId");

            // Set up a handler for when the custom option item's value is changed
            advancedPrintOptions.OptionChanged += async (o, e) =>
            {
                if (e.OptionId == null) return;
                var changedOptionId = e.OptionId.ToString();
                if (changedOptionId == "ColorId")
                {
                    await Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
                    {
                        // Force the preview to be re-calculated
                        _printDocument.InvalidatePreview();
                    });
                }
            };
        }

        private List<UIElement> _printPages = new List<UIElement>();

        // This handler for the Paginate event is raised to request the count of preview pages
        private void OnRequestPrintPreviewPages(Object sender, PaginateEventArgs e)
        {
            _printPages.Clear();

            #region Prepare Preview Page Content
            // Obtain the custom property value to use in pagination calculations
            var printTaskOptionDetails = PrintTaskOptionDetails.GetFromPrintTaskOptions(e.PrintTaskOptions);
            var colorIdOptions = printTaskOptionDetails.Options["ColorId"];
            var colorId = colorIdOptions.Value.ToString();
            var selectedColor =
                colorId == "RedId" ? Colors.Red :
                colorId == "GreenId" ? Colors.Green :
                colorId == "BlueId" ? Colors.Blue :
                Colors.Black;
            
            // Gets the attributes that describe the printer page
            //var pageDescription = e.PrintTaskOptions.GetPageDescription(0);
            //pageDescription.DpiX, pageDescription.DpiY, pageDescription.ImageableRect, pageDescription.PageSize

            //var totalPages = new Random((Int32)DateTimeOffset.Now.Ticks).Next(10) + 1;
            var totalPages = 3;
            for (Int32 i = 0; i < totalPages; i++)
            {
                var textBlock = new TextBlock();
                textBlock.Foreground = new SolidColorBrush(Colors.Black);
                textBlock.Text = (i + 1).ToString();                
                var viewBox = new Viewbox();
                viewBox.Child = textBlock;
                var border = new Border();
                border.Child = viewBox;
                border.Background = new SolidColorBrush(selectedColor);
                var page = new UserControl();
                page.HorizontalAlignment = Windows.UI.Xaml.HorizontalAlignment.Stretch;
                page.VerticalAlignment = Windows.UI.Xaml.VerticalAlignment.Stretch;
                page.Content = border;

                _printPages.Add(page);
            }

            #endregion
            
            _printDocument.SetPreviewPageCount(_printPages.Count(), PreviewPageCountType.Final); 
            // can also be Intermediate to indicate the count is based on non-final pages
        }

        // This requests/receives a specific print preview page
        private void OnGetPreviewPage(Object sender, GetPreviewPageEventArgs e)
        {
            // e.PageNumber is the 1-based page number for the page to be displayed 
            // in the preview panel
            _printDocument.SetPreviewPage(e.PageNumber, _printPages[e.PageNumber - 1]);
        }

        // This is called when the system is ready to start printing and provides the final pages
        private void OnAddPrintPages(Object sender, AddPagesEventArgs e)
        {
            var finalPages = new List<UIElement>();

            #region Prepare Final Page Content
            finalPages = _printPages;
            #endregion

            foreach (var finalPage in finalPages)
            {
                _printDocument.AddPage(finalPage);
            }
            _printDocument.AddPagesComplete();
        }
    }
}
