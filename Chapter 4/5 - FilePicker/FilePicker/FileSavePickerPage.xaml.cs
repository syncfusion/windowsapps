﻿using System;
using Windows.ApplicationModel.Activation;
using Windows.Storage;
using Windows.Storage.Pickers.Provider;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class FileSavePickerPage : Page
    {
        public FileSavePickerPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        private FileSavePickerUI _fileSavePickerUI;

        public void Activate(FileSavePickerActivatedEventArgs args)
        {
            _fileSavePickerUI = args.FileSavePickerUI;
            _fileSavePickerUI.TargetFileRequested += HandleTargetFileRequested;
            Window.Current.Content = this;
            Window.Current.Activate();
        }

        private async void HandleTargetFileRequested(FileSavePickerUI sender, TargetFileRequestedEventArgs args)
        {
            // Request a deferral to accomodate the async file creation
            var deferral = args.Request.GetDeferral();

            // Create the file in ApplicationData to be returned as the user's selection.
            var localFolder = ApplicationData.Current.LocalFolder; 
            args.Request.TargetFile = await localFolder.CreateFileAsync(sender.FileName, CreationCollisionOption.GenerateUniqueName);

            // Complete the deferral
            deferral.Complete();
        }
    }
}
