﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="navigationParameter">The parameter value passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested.
        /// </param>
        /// <param name="pageState">A dictionary of state preserved by this page during an earlier
        /// session.  This will be null the first time a page is visited.</param>
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="pageState">An empty dictionary to be populated with serializable state.</param>
        protected override void SaveState(Dictionary<String, Object> pageState)
        {
        }

        private IStorageFile _fileToSave;

        private async void HandleOpenFileClicked(Object sender, RoutedEventArgs e)
        {
            var picker = new FileOpenPicker();
            picker.FileTypeFilter.Add(".txt");
            picker.FileTypeFilter.Add(".jpg");
            picker.FileTypeFilter.Add(".png");

            // Single file selection
            //var firstFile = await picker.PickSingleFileAsync();

            // Multiple file selection
            var selectedFiles = await picker.PickMultipleFilesAsync();
            var firstFile = selectedFiles.FirstOrDefault();
            if (firstFile != null)
            {
                _fileToSave = firstFile;
                ShowSeletedPicture(firstFile);
            }
        }

        private async void ShowSeletedPicture(IStorageFile fileToShow)
        {
            var imageStream = await fileToShow.OpenReadAsync();
            var image = new BitmapImage();
            image.SetSource(imageStream);
            SelectedImage.Source = image;
        }

        private async void HandleSaveFileClicked(Object sender, RoutedEventArgs e)
        {
            if (_fileToSave == null) return;
            var picker = new FileSavePicker();
            picker.SuggestedFileName = "SamplePic";
            picker.FileTypeChoices.Add("Image Files", new List<String>(new[] { ".png", ".jpg", ".bmp" }));
            var chosenFile = await picker.PickSaveFileAsync();
            if (chosenFile != null)
            {
                await _fileToSave.CopyAndReplaceAsync(chosenFile);
            }
        }

    }
}
