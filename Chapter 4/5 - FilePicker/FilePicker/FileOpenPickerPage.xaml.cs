﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.ApplicationModel.Activation;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Pickers.Provider;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

// The File Open Picker Contract item template is documented at http://go.microsoft.com/fwlink/?LinkId=234239

namespace WindowsStoreAppsSuccinctly
{
    /// <summary>
    /// This page displays files owned by the application so that the user can grant another application
    /// access to them.
    /// </summary>
    public sealed partial class FileOpenPickerPage : WindowsStoreAppsSuccinctly.Common.LayoutAwarePage
    {
        /// <summary>
        /// Files are added to or removed from the Windows UI to let Windows know what has been selected.
        /// </summary>
        private Windows.Storage.Pickers.Provider.FileOpenPickerUI _fileOpenPickerUI;

        public FileOpenPickerPage()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Invoked when another application wants to open files from this application.
        /// </summary>
        /// <param name="args">Activation data used to coordinate the process with Windows.</param>
        public void Activate(FileOpenPickerActivatedEventArgs args)
        {
            this._fileOpenPickerUI = args.FileOpenPickerUI;
            _fileOpenPickerUI.FileRemoved += FilePickerUI_FileRemoved;

            this.DefaultViewModel["CanGoUp"] = false;
            Window.Current.Content = this;
            Window.Current.Activate();

            // Custom code to populate the UI with the files in the currently displayed context
            DisplayCurrentFolderFiles();
        }

        private async void DisplayCurrentFolderFiles()
        {
            var storageFolder = ApplicationData.Current.LocalFolder;
            var savedFiles = await storageFolder.GetFilesAsync();

            var sampleFiles = new List<SampleFile>();

            foreach(var savedFile in savedFiles)
            {
                var thumbnail = await savedFile.GetThumbnailAsync(ThumbnailMode.PicturesView);
                var image = new BitmapImage();
                image.SetSource(thumbnail);
                sampleFiles.Add(new SampleFile
                {
                    Image = image,
                    Title = savedFile.DisplayName,
                    Description = "This is a saved sample file",
                    BackingFile = savedFile,
                });
            }
            this.DefaultViewModel["Files"] = sampleFiles;
        }

        private class SampleFile
        {
            public ImageSource Image { get; set; }
            public String Title { get; set; }
            public String Description { get; set; }
            public IStorageFile BackingFile { get; set; }
        }

        /// <summary>
        /// Invoked when user removes one of the items from the Picker basket
        /// </summary>
        /// <param name="sender">The FileOpenPickerUI instance used to contain the available files.</param>
        /// <param name="args">Event data that describes the file removed.</param>
        private async void FilePickerUI_FileRemoved(FileOpenPickerUI sender, FileRemovedEventArgs args)
        {
            // Ensure the call occurs on the UI thread
            await Dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                // Find the GridView item that matches the removed item from the Picker UI selection list
                var selectedFiles = fileGridView.SelectedItems.Cast<SampleFile>();
                var removedSelectedFile = selectedFiles.FirstOrDefault(x => x.Title == args.Id);
                if (removedSelectedFile != null)
                {
                    fileGridView.SelectedItems.Remove(removedSelectedFile);
                }
            });
        }

        /// <summary>
        /// Invoked when the selected collection of files changes.
        /// </summary>
        /// <param name="sender">The GridView instance used to display the available files.</param>
        /// <param name="e">Event data that describes how the selection changed.</param>
        private void FileGridView_SelectionChanged(Object sender, SelectionChangedEventArgs e)
        {
            // Add any newly selected files in the UI to the Picker
            foreach (var addedFile in e.AddedItems.Cast<SampleFile>())
            {
                if (_fileOpenPickerUI.CanAddFile(addedFile.BackingFile))
                {
                    _fileOpenPickerUI.AddFile(addedFile.Title, addedFile.BackingFile);
                }
            }

            // Remove deselected items from the Picker
            foreach (var removedFile in e.RemovedItems.Cast<SampleFile>())
            {
                _fileOpenPickerUI.RemoveFile(removedFile.Title);
            }
        }

        /// <summary>
        /// Invoked when the "Go up" button is clicked, indicating that the user wants to pop up
        /// a level in the hierarchy of files.
        /// </summary>
        /// <param name="sender">The Button instance used to represent the "Go up" command.</param>
        /// <param name="args">Event data that describes how the button was clicked.</param>
        private void GoUpButton_Click(object sender, RoutedEventArgs args)
        {
            // TODO: Set this.DefaultViewModel["CanGoUp"] to true to enable the corresponding command,
            //       use updates to this.DefaultViewModel["Files"] to reflect file hierarchy traversal
        }
    }
}
